1. oracle查看sql执行时间
sqlplus登录，然后执行set timing on，再执行具体的sql即可。


count= 586554
sql耗费时间为：61ms
Create Index houseroom_order On HOUSEROOMSHAPE(ROOMORDER);
sql耗费时间为：3ms

查看sql执行计划

```sql
explain plan for
SELECT length,width from HOUSEROOMSHAPE where roomorder = 259

select * from table(dbms_xplan.display)
```

## findWSPubGoodsMaterialV2 sql耗费时间为：388ms
优化后：
- Create Index MATERIAL_INN_ID_ind On INNER_MATERIAL(INN_ID);
findWSPubGoodsMaterialV2 sql耗费时间为：272ms

```sql
	AND ( IM.CLASS_TYPE IN ( SELECT m_class FROM Inner_Layout_Pub WHERE b_id = 56 GROUP BY m_class ) OR IM.M_CLASS_CODE = 'chuangtoubeijing' ) 


	AND ( IM.CLASS_TYPE IN ( SELECT m_class FROM Inner_Layout_Pub WHERE b_id = 56 GROUP BY m_class union (select 'chuangtoubeijing' from dual) ) ) 
```
findWSPubGoodsMaterialV2 sql耗费时间为：51ms

```sql
explain plan for
SELECT DISTINCT
	( IM.ID ) IM_ID,
	regexp_substr( IG.GOOD_SIZE, '[^*]+', 1 ) AS LENGTH,
	IM.INN_ID,
	IM.P_ID,
	IG.M_CLASS G_CLASS,
	IM.M_CLASS_CODE M_CLASS_CODE,
	IM.GOOD_CODE,
	IM.VISUALANGLE,
	IM.CLASS_TYPE, 'https://file.zk-house.com/fileserver' || IM.IMG_PATH AS IMG_PATH,
	IM.POSITION,
	IG.GOOD_NAME,
	IG.BRAND_ID,
	IG.BARND_NAME,
	IG.ALLTYPES, 'https://file.zk-house.com/fileserver' || IG.M_IMG_PATH AS M_IMG_PATH,
	IG.GOOD_SIZE,
	IM.example_img_id,
	GI.COORDINATE,
	GI.TYPE AS BJTYPE 
FROM
	INNER_GOODS IG INNER join INNER_MATERIAL IM ON IM.GOOD_CODE = IG.GOOD_CODE LEFT JOIN INNER_GOODS_IMGES GI ON IM.GOOD_CODE = GI.GOOD_CODE 
	AND IM.VISUALANGLE = GI.VISUALANGLE 
	AND IM.M_CLASS_CODE = GI.CLASS_TYPE 
	AND GI.ROOM_FUNCTION = 'WS'
WHERE
	IG.TRUE_GOODS = '1' 
	AND IG.STATUS = '1' 
	AND IM.INN_ID = 56 
	AND ( IM.CLASS_TYPE IN ( SELECT m_class FROM Inner_Layout_Pub WHERE b_id = 56 GROUP BY m_class ) OR IM.M_CLASS_CODE = 'chuangtoubeijing' ) 
	AND IM.POSITION IN ( '1', '2' ) 
	AND IM.GOOD_CODE IN (
SELECT
	t.good_code 
FROM
	INNER_MATCHES_IMG_REL t left join inner_matches m ON t.m_id = m.id 
WHERE
	m.style_id = 191
	AND m.room_function = 'WS' UNION
SELECT
	xc.good_code 
FROM
	inner_xc_goods xc left join INNER_XC_STYLE xs ON xc.xc_id = xs.xc_id 
WHERE
	xs.s_id =191
	AND xc.room_function = 'WS' UNION
SELECT
	sr.good_code 
FROM
	INNER_STYLE_IMG_REL sr 
WHERE
	sr.s_id = 191
	AND sr.room_function = 'WS' UNION
SELECT
	icg.good_code 
FROM
	Inner_Common_Goods icg left join Inner_Common_Style ics ON icg.common_id = ics.common_id 
WHERE
	ics.s_id = 191
	AND icg.room_function = 'WS' 
	) 
	AND IM.M_CLASS_CODE IN (
SELECT
	t.m_class 
FROM
	INNER_MATCHES_IMG_REL t left join inner_matches m ON t.m_id = m.id 
WHERE
	m.style_id = 191
	AND m.room_function = 'WS' UNION
SELECT
	xc.m_class_code AS m_class 
FROM
	inner_xc_goods xc left join INNER_XC_STYLE xs ON xc.xc_id = xs.xc_id 
WHERE
	xs.s_id =191
	AND xc.room_function = 'WS' UNION
SELECT
	sr.m_class 
FROM
	INNER_STYLE_IMG_REL sr 
WHERE
	sr.s_id = 191
	AND sr.room_function = 'WS' UNION
SELECT
	icg.m_class_code AS m_class 
FROM
	Inner_Common_Goods icg left join Inner_Common_Style ics ON icg.common_id = ics.common_id 
WHERE
	ics.s_id = 191
	AND icg.room_function = 'WS' 
	)
	
select * from table(dbms_xplan.display);
```










## findWithoutMaterialByStyleV2 sql耗费时间为：418ms

优化后：
findWithoutMaterialByStyleV2 sql耗费时间为：65ms
```sql
select id+l from SYS_ROLE,(SELECT LEVEL l FROM DUAL CONNECT BY LEVEL< 100)b where l <= ID

select REPLACE ( 'sd,ss', ',') from dual 

# 最多4个，所以没有必要用1-100计算，1-10即可
 select M_CLASS,good_code,LENGTH(m_class ) - LENGTH(REPLACE ( m_class, ',' )) + 1 as m_len from INNER_GOODS ORDER BY m_len desc

 SELECT
	regexp_substr( IG.GOOD_SIZE, '[^*]+', 1 ) AS LENGTH,
	IG.good_code,
	IG.m_class,
	REGEXP_SUBSTR( IG.m_class, '[^,]+', 1, l ) AS m_class_code,
	REGEXP_SUBSTR( IG.m_class, '[^,]+', 1, l ) AS CLASS_TYPE,
	IG.GOOD_CODE,
	'' AS IM_ID,
	'' AS INN_ID,
	'' AS P_ID,
	'' AS VISUALANGLE,
	'' AS IMG_PATH,
	'' AS POSITION,
	IG.GOOD_NAME,
	IG.BRAND_ID,
	IG.BARND_NAME,
	IG.ALLTYPES, 'https://file.zk-house.com/fileserver' || IG.M_IMG_PATH AS M_IMG_PATH,
	IG.GOOD_SIZE,
	'' AS example_img_id,
	'' AS COORDINATE,
	'0' AS fit_flag 
FROM
	INNER_GOODS IG,
	( SELECT LEVEL l FROM DUAL CONNECT BY LEVEL <= 10 ) b 
WHERE
	l <= LENGTH( IG.m_class ) - LENGTH(
	REPLACE ( m_class, ',' )) + 1 
	AND IG.good_code IN (
SELECT
	t.good_code 
FROM
	INNER_MATCHES_IMG_REL t left join inner_matches m ON t.m_id = m.id 
WHERE
	m.style_id =191 
	AND m.room_function ='WS' UNION
SELECT
	xc.good_code 
FROM
	inner_xc_goods xc left join INNER_XC_STYLE xs ON xc.xc_id = xs.xc_id 
WHERE
	xs.s_id =191 
	AND xc.room_function ='WS' UNION
SELECT
	sr.good_code 
FROM
	INNER_STYLE_IMG_REL sr 
WHERE
	sr.s_id = 191
	AND sr.room_function = 'WS' UNION
SELECT
	icg.good_code 
FROM
	Inner_Common_Goods icg left join Inner_Common_Style ics ON icg.common_id = ics.common_id 
WHERE
	ics.s_id = 191
	AND icg.room_function = 'WS'
	) 
	AND IG.good_code NOT IN (
SELECT
	im.good_code 
FROM
	INNER_MATERIAL IM LEFT JOIN INNER_BASKET_SHAPE BS ON IM.POSITION = BS.POSITION 
WHERE
	IM.good_code IN (
SELECT
	t.good_code 
FROM
	INNER_MATCHES_IMG_REL t left join inner_matches m ON t.m_id = m.id 
WHERE
	m.style_id =191
	AND m.room_function ='WS' UNION
SELECT
	xc.good_code 
FROM
	inner_xc_goods xc left join INNER_XC_STYLE xs ON xc.xc_id = xs.xc_id 
WHERE
	xs.s_id =191 
	AND xc.room_function ='WS' UNION
SELECT
	sr.good_code 
FROM
	INNER_STYLE_IMG_REL sr 
WHERE
	sr.s_id = 191
	AND sr.room_function = 'WS' UNION
SELECT
	icg.good_code 
FROM
	Inner_Common_Goods icg left join Inner_Common_Style ics ON icg.common_id = ics.common_id 
WHERE
	ics.s_id =191
	AND icg.room_function = 'WS' 
	) 
	AND IM.INN_ID =56
	AND IM.POSITION IN ( '1', '2' ) 
GROUP BY
	im.good_code 
	)
```


## findWSKaobaoGoodsMaterialV2 sql耗费时间为：167ms
优化后

findWSKaobaoGoodsMaterialV2 sql耗费时间为：57ms
```sql
select count(DISTINCT
	( IM_ID )) from (

SELECT DISTINCT
	( IM.ID ) AS IM_ID,
	regexp_substr ( IG.GOOD_SIZE, '[^*]+', 1 ) AS LENGTH,
	IM.INN_ID,
	IM.P_ID,
	IG.M_CLASS G_CLASS,
	IM.M_CLASS_CODE M_CLASS_CODE,
	IM.GOOD_CODE,
	IM.VISUALANGLE,
	IM.CLASS_TYPE,
	'https://file.zk-house.com/fileserver' || IM.IMG_PATH AS IMG_PATH,
	IM.POSITION,
	IG.GOOD_NAME,
	IG.BRAND_ID,
	IG.BARND_NAME,
	IG.alltypes,
	'https://file.zk-house.com/fileserver' || IG.M_IMG_PATH AS M_IMG_PATH,
	IG.GOOD_SIZE,
	IM.example_img_id,
	GI.COORDINATE,
	GI.TYPE AS BJTYPE,
	'1' AS fit_flag 
FROM
	INNER_MATERIAL IM
	INNER JOIN INNER_GOODS IG ON IM.GOOD_CODE = IG.GOOD_CODE
	LEFT JOIN INNER_GOODS_IMGES GI ON IM.GOOD_CODE = GI.GOOD_CODE 
	AND IM.VISUALANGLE = GI.VISUALANGLE 
	AND IM.M_CLASS_CODE = GI.CLASS_TYPE 
	AND GI.ROOM_FUNCTION = 'WS' 
WHERE
	IG.TRUE_GOODS = '1' 
	AND IG.STATUS = '1' 
	AND IM.m_Class_Code = 'kaobaobaozhen' 
	AND IM.POSITION IN ( '1', '2' ) 
	AND IM.INN_ID = 56 
	AND IM.GOOD_CODE IN (
		SELECT
			t.good_code 
		FROM
			INNER_MATCHES_IMG_REL t
			LEFT JOIN inner_matches m ON t.m_id = m.id 
		WHERE
			m.style_id IN 191 
			AND m.room_function = 'WS' UNION
		SELECT
			xc.good_code 
		FROM
			inner_xc_goods xc
			LEFT JOIN INNER_XC_STYLE xs ON xc.xc_id = xs.xc_id 
		WHERE
			xs.s_id IN 191 
			AND xc.room_function = 'WS' UNION
		SELECT
			sr.good_code 
		FROM
			INNER_STYLE_IMG_REL sr 
		WHERE
			sr.s_id IN 191 
			AND sr.room_function = 'WS' UNION
		SELECT
			icg.good_code 
		FROM
			Inner_Common_Goods icg
			LEFT JOIN Inner_Common_Style ics ON icg.common_id = ics.common_id 
		WHERE
			ics.s_id IN 191 
			AND icg.room_function = 'WS' 
	) 
	AND IM.P_ID IN (

		SELECT
			sr.good_code 
		FROM
			INNER_STYLE_IMG_REL sr 
		WHERE
			sr.s_id IN 191 
			AND sr.room_function = 'WS' 
	) union 
	
	SELECT DISTINCT
	( IM.ID ) AS IM_ID,
	regexp_substr ( IG.GOOD_SIZE, '[^*]+', 1 ) AS LENGTH,
	IM.INN_ID,
	IM.P_ID,
	IG.M_CLASS G_CLASS,
	IM.M_CLASS_CODE M_CLASS_CODE,
	IM.GOOD_CODE,
	IM.VISUALANGLE,
	IM.CLASS_TYPE,
	'https://file.zk-house.com/fileserver' || IM.IMG_PATH AS IMG_PATH,
	IM.POSITION,
	IG.GOOD_NAME,
	IG.BRAND_ID,
	IG.BARND_NAME,
	IG.alltypes,
	'https://file.zk-house.com/fileserver' || IG.M_IMG_PATH AS M_IMG_PATH,
	IG.GOOD_SIZE,
	IM.example_img_id,
	GI.COORDINATE,
	GI.TYPE AS BJTYPE,
	'1' AS fit_flag 
FROM
	INNER_MATERIAL IM
	INNER JOIN INNER_GOODS IG ON IM.GOOD_CODE = IG.GOOD_CODE
	LEFT JOIN INNER_GOODS_IMGES GI ON IM.GOOD_CODE = GI.GOOD_CODE 
	AND IM.VISUALANGLE = GI.VISUALANGLE 
	AND IM.M_CLASS_CODE = GI.CLASS_TYPE 
	AND GI.ROOM_FUNCTION = 'WS' 
WHERE
	IG.TRUE_GOODS = '1' 
	AND IG.STATUS = '1' 
	AND IM.m_Class_Code = 'kaobaobaozhen' 
	AND IM.POSITION IN ( '1', '2' ) 
	AND IM.INN_ID = 56 
	AND IM.GOOD_CODE IN (
		SELECT
			t.good_code 
		FROM
			INNER_MATCHES_IMG_REL t
			LEFT JOIN inner_matches m ON t.m_id = m.id 
		WHERE
			m.style_id IN 191 
			AND m.room_function = 'WS' UNION
		SELECT
			xc.good_code 
		FROM
			inner_xc_goods xc
			LEFT JOIN INNER_XC_STYLE xs ON xc.xc_id = xs.xc_id 
		WHERE
			xs.s_id IN 191 
			AND xc.room_function = 'WS' UNION
		SELECT
			sr.good_code 
		FROM
			INNER_STYLE_IMG_REL sr 
		WHERE
			sr.s_id IN 191 
			AND sr.room_function = 'WS' UNION
		SELECT
			icg.good_code 
		FROM
			Inner_Common_Goods icg
			LEFT JOIN Inner_Common_Style ics ON icg.common_id = ics.common_id 
		WHERE
			ics.s_id IN 191 
			AND icg.room_function = 'WS' 
	) 
	AND IM.P_ID IN (
		SELECT
			t.good_code 
		FROM
			INNER_MATCHES_IMG_REL t
			LEFT JOIN inner_matches m ON t.m_id = m.id 
		WHERE
			m.style_id IN 191 
			AND m.room_function = 'WS' 
	) 
)
```



diyDao.getBasket(_basketId) sql重复

字符串拼接使用StringBuilder 
- String classType = "'A','B','C','D',";



findNewBasket和findAllGoodsMat.findWsGoods
if (StringUtils.isNotBlank(shape))是否可以合并。for循环。代码复用性。
diyMapper.findNewDoororwinShapeV2(string)重复查库


根据实际进深计算，适合标记 fit_flag

这里是否需要前端来计算。

### in exists
有两个简单例子，以说明 “exists”和“in”的效率问题

1) select * from T1 where exists(select * from T2 where T1.a=T2.a) ;

    T1数据量小而T2数据量非常大时，T1<<T2 时，1) 的查询效率高。

2) select * from T1 where T1.a in (select T2.a from T2) ;

     T1数据量非常大而T2数据量小时，T1>>T2 时，2) 的查询效率高。


regexp_substr(IG.GOOD_SIZE, '[^*]+', 1) as LENGTH,
有时候不是数字。
```xml
java.lang.NumberFormatException: For input string: "D350"
	at java.lang.NumberFormatException.forInputString(NumberFormatException.java:65)
	at java.lang.Integer.parseInt(Integer.java:580)
	at java.lang.Integer.parseInt(Integer.java:615)
	at com.gaobiao.util.Util.parseInteger(Util.java:913)
	at com.gaobiao.service.GoodsService.setFitFlagBySize(GoodsService.java:521)
	at com.gaobiao.service.GoodsService.findWsGoods(GoodsService.java:467)
	at com.gaobiao.service.GoodsService.findAllGoodsMat(GoodsService.java:350)
	at com.gaobiao.service.GoodsService.findAllGoodsMat(GoodsService.java:46)
	at com.gaobiao.service.impl.DIYV2ServiceImpl.getDiyStyles(DIYV2ServiceImpl.java:131)
	at com.gaobiao.controller.DiyV2Controller.getDiyStyles(DiyV2Controller.java:39)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:498)
	at org.springframework.web.method.support.InvocableHandlerMethod.doInvoke(InvocableHandlerMethod.java:205)
	at org.springframework.web.method.support.InvocableHandlerMethod.invokeForRequest(InvocableHandlerMethod.java:133)
	at org.springframework.web.servlet.mvc.method.annotation.ServletInvocableHandlerMethod.invokeAndHandle(ServletInvocableHandlerMethod.java:97)
	at org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.invokeHandlerMethod(RequestMappingHandlerAdapter.java:827)
	at org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.handleInternal(RequestMappingHandlerAdapter.java:738)
	at org.springframework.web.servlet.mvc.method.AbstractHandlerMethodAdapter.handle(AbstractHandlerMethodAdapter.java:85)
	at org.springframework.web.servlet.DispatcherServlet.doDispatch(DispatcherServlet.java:967)
	at org.springframework.web.servlet.DispatcherServlet.doService(DispatcherServlet.java:901)
	at org.springframework.web.servlet.FrameworkServlet.processRequest(FrameworkServlet.java:970)
	at org.springframework.web.servlet.FrameworkServlet.doPost(FrameworkServlet.java:872)
	at javax.servlet.http.HttpServlet.service(HttpServlet.java:707)
	at org.springframework.web.servlet.FrameworkServlet.service(FrameworkServlet.java:846)
```
```json
{
    "casebay": 4200,
    "depth": 4000,
    "roomFunction": "ZW",
    "styleId": 188,
    "shape": "ZW_S01_X_516040,ZW_CT_PC_516040,ZW_MT_2M19_516040"
}
```