

## guava使用总结

https://www.jianshu.com/p/df81fe1409f0

源码包简单说明：

- com.google.common.annotations：普通注解类型。
- com.google.common.base：基本工具类库和接口。
- com.google.common.cache：缓存工具包，非常简单易用且功能强大的JVM内缓存。
- com.google.common.collect：带泛型的集合接口扩展和实现，以及工具类，这里你会发现很多好玩的集合。
- com.google.common.eventbus：发布订阅风格的事件总线。
- com.google.common.hash： 哈希工具包。
- com.google.common.io：I/O工具包。
- com.google.common.math：原始算术类型和超大数的运算工具包。

- com.google.common.net：网络工具包。
- com.google.common.primitives：八种原始类型和无符号类型的静态工具包。
- com.google.common.reflect：反射工具包。
- com.google.common.util.concurrent：多线程工具包。

### 1、guava-utilities

#### 1.1 连接器Joiner

将集合中的元素，通过设置分隔符，拼接成一个字符串；

- on("x")，x表示分隔符；

- skipNulls()，忽略空，就不会报错空指针；

  - ```java
    stream().filter(item -> item != null && !item.isEmpty()).collect(joining("#"));
    ```

- useForNull("y")，给null对象提供一个默认值；

  - ```java
    stream().map(this::default).collect(joining("#"))
    String default(String item) {
        return item == null || item.isEmpty() ? "DEFAULT" : item;
    }
    ```

- appendTo(builder, list)，将集合中的元素拼接到StringBuilder对象中；

- apendTo(fileWriter, list)，将集合中的元素拼接到文件中；

- 对map进行操作

  - ```java
    Map<String, String> stringMap = of("Hello", "Guava", "Java", "Scala");
    assertThat(Joiner.on('#').withKeyValueSeparator("=").join(stringMap), equalTo("Hello=Guava#Java=Scala"));
    ```

#### 1.2 拆分器Splitter

将一个字符串，根据分隔符，截取成集合（不包括分隔符）。

- on("x")，x表示分隔符；

- splitToList(String s)，“”字符串也会放入集合；

  - ```java
    List<String> result = Splitter.on("|").splitToList("hello|world");
    assertThat(result, notNullValue());
    assertThat(result.size(), equalTo(2));
    assertThat(result.get(0), equalTo("hello"));
    assertThat(result.get(1), equalTo("world"));
    ```

- omitEmptyStrings()，“”字符串不会放入集合；

  - ```java
    List<String> result = Splitter.on("|").splitToList("hello|world|||");
    assertThat(result, notNullValue());
    assertThat(result.size(), equalTo(5));
    
    result = Splitter.on("|").omitEmptyStrings().splitToList("hello|world|||");
    assertThat(result, notNullValue());
    assertThat(result.size(), equalTo(2));
    ```

- trimResults()，“ ”空字符串不会放入集合中；

  - ```java
    List<String> result = Splitter.on("|").omitEmptyStrings().splitToList("hello | world|||");
    assertThat(result, notNullValue());
    assertThat(result.size(), equalTo(2));
    assertThat(result.get(0), equalTo("hello "));
    assertThat(result.get(1), equalTo(" world"));
    
    result = Splitter.on("|").trimResults().omitEmptyStrings().splitToList("hello | world|||");
    assertThat(result.get(0), equalTo("hello"));
    assertThat(result.get(1), equalTo("world"));
    ```

- fixedLength(int len)，按照固定长度截取字符串，放入集合中；

  - ```java
    List<String> result = Splitter.fixedLength(4).splitToList("aa1abbbbccccdddd");
    assertThat(result, notNullValue());
    assertThat(result.size(), equalTo(4));
    assertThat(result.get(0), equalTo("aa1a"));
    assertThat(result.get(3), equalTo("dddd"));
    ```

- limit(int size)，将字符串截取成size个子字符串，放入集合；

  - ```java
    List<String> result = Splitter.on("#").limit(3).splitToList("hello#world#java#google#scala");
    assertThat(result, notNullValue());
    assertThat(result.size(), equalTo(3));
    assertThat(result.get(0), equalTo("hello"));
    assertThat(result.get(1), equalTo("world"));
    assertThat(result.get(2), equalTo("java#google#scala"));
    ```

- 正则匹配 分隔符

  - ```java
    List<String> result = Splitter.on(Pattern.compile("\\|")).trimResults().omitEmptyStrings().splitToList("hello | world|||");
    assertThat(result, notNullValue());
    assertThat(result.size(), equalTo(2));
    assertThat(result.get(0), equalTo("hello"));
    assertThat(result.get(1), equalTo("world"));
    ```

- 特殊字符串转换成map集合

  - ```java
    Map<String, String> result = Splitter.on(Pattern.compile("\\|")).trimResults()
            .omitEmptyStrings().withKeyValueSeparator("=").split("hello=HELLO| world=WORLD|||");
    assertThat(result, notNullValue());
    assertThat(result.size(), equalTo(2));
    assertThat(result.get("hello"),equalTo("HELLO"));
    assertThat(result.get("world"),equalTo("WORLD"));
    ```



#### 1.3 Strings

- ```java
  assertThat(Strings.emptyToNull(""), nullValue());
  assertThat(Strings.nullToEmpty(null), equalTo(""));
  assertThat(Strings.nullToEmpty("hello"), equalTo("hello"));
  assertThat(Strings.commonPrefix("Hello", "Hit"), equalTo("H"));
  assertThat(Strings.commonPrefix("Hello", "Xit"), equalTo(""));
  assertThat(Strings.commonSuffix("Hello", "Echo"), equalTo("o"));
  assertThat(Strings.repeat("Alex", 3), equalTo("AlexAlexAlex"));
  assertThat(Strings.isNullOrEmpty(null), equalTo(true));
  assertThat(Strings.isNullOrEmpty(""), equalTo(true));
  assertThat(Strings.padStart("Alex", 3, 'H'), equalTo("Alex"));
  assertThat(Strings.padStart("Alex", 6, 'H'), equalTo("HHAlex"));
  assertThat(Strings.padEnd("Alex", 5, 'H'), equalTo("AlexH"));
  ```

### 2、集合类操作

#### 2.1 Base64Encode和Base64Decode

```JAVA
@Test
public void testBase64Encode() {
    BaseEncoding baseEncoding = BaseEncoding.base64();
    System.out.println(baseEncoding.encode("hello".getBytes()));
    System.out.println(baseEncoding.encode("a".getBytes()));
}

@Test
public void testBase64Decode() {
    BaseEncoding baseEncoding = BaseEncoding.base64();
    System.out.println(new String(baseEncoding.decode("aGVsbG8=")));
}
```



### 3、guava-cache

Guava Cache是一个全内存的本地缓存实现，它提供了线程安全的实现机制。整体上来说Guava cache 是本地缓存的[不二之选](https://www.baidu.com/s?wd=%E4%B8%8D%E4%BA%8C%E4%B9%8B%E9%80%89&tn=24004469_oem_dg&rsv_dl=gh_pl_sl_csd)，简单易用，性能好。

使用场景

- 你愿意消耗一部分内存来提升速度；
- 你已经预料某些值会被多次调用； 
- 缓存数据不会超过内存总量，适合少量数据的缓存；

https://www.imooc.com/article/34924

怎么用

- 设置缓存容量
- 设置超时时间
- 提供移除监听器
- 提供缓存加载器
- 构建缓存



