## 基于jenkins构建持续集成环境

### 1 jenkins部署与环境安装

1.1 jenkins下载，并上传到/srv/packages/ 

> https://jenkins.io/download
> 
> jenkins.war
> 
> jdk8支持

1.2 下载tomcat8
   
   wget http://mirrors.shu.edu.cn/apache/tomcat/tomcat-8/v8.5.33/bin/apache-tomcat-8.5.33.tar.gz

```shell
    tar -zxvf apache-tomcat-8.5.33.tar.gz
    mv apache-tomcat-8.5.33 ../jenkins-tomcat
    cp jenkins.war ../jenkins-tomcat/webapps/
    vi /srv/jenkins-tomcat/conf/server.xml
        <Server port="8095" shutdown="SHUTDOWN"> 

        <Connector executor="tomcatThreadPool"
               port="8090" protocol="HTTP/1.1"
               connectionTimeout="20000"
               redirectPort="8493" />

        <Connector port="8094" protocol="AJP/1.3" redirectPort="8493" />

    /srv/jenkins-tomcat/bin/startup.sh 

    systemctl stop firewalld.service 
```
1.3 访问 http://192.168.109.122:8090/jenkins
，登陆解锁jenkins
   
   ![login](./images/jenkins.png)

1.4 选择安装推荐的插件（点击按钮即可）

1.5 创建用户，然后开始jenkins之旅

1.6 安装两个插件（maven、deploy）

> 系统管理 -> 管理插件 -> 可选插件
> 
> 搜索 maven invoker 和 deploy to container，直接安装

1.7 新建任务,让jenkins构建一个项目包

> 构建一个maven项目


> 构建成功之，配置

* 项目描述
* 源码管理 -> git -> url -> gitlab 用户名和密码
  id和描述任意
* Build -> Goals and options:clean install
* 点击保存、应用

> 安装 maven 和 git

http://maven.apache.org/download.cgi
apache-maven-3.5.4-bin.tar

```shell
  tar -zxvf apache-maven-3.5.4-bin.tar
  mv apache-maven-3.5.4 ..

  // 添加环境变量
  /srv/apache-maven-3.5.4
  vim /etc/profile

  export PATH=$PATH:/srv/apache-maven-3.5.4/bin

  source /etc/profile
  mvn --version
```

https://github.com/git/git
选择tag，下载zip

```java
  wget https://github.com/git/git/archive/v2.3.0.zip

  unzip git-2.3.0
  mv git-2.3.0 ..
  ln -s git-2.3.0 git
  // 编译
  yum -y install curl-devel expat-devel gettext-devel openssl-devel zlib-devel gcc perl-ExtUtils-MakeMaker

  cd git
  make prefix=/usr/local/git all
  make prefix=/usr/local/git install

  vim /etc/profile

  export PATH=/svr/git/bin:$PATH

  source /etc/profile
  git --version
```

  * 重新访问jenkins页面，配置。安装maven，去掉自动安装，添加MAVEN_HOME=/srv/apache-maven-3.5.4/
  * 点击立即构建

https://www.cnblogs.com/xinzhiyan/p/8748520.html

1.8 构建项目包后，将其放入maven私服和tomcat容器。

> jenkins 配置 -> 构建后步骤 -> 
> maven准备和tomcat准备

![warToNexus.png](./images/warToNexus.png)

安装tomcat 配置用户信息

```java
  vim tomcat-users.xml

  <role rolename="manager-gui"/>
  <role rolename="manager-script"/>
  <role rolename="manager-jmx"/>
  <role rolement="manager-status"/>
  <user username="tomcat" password="123456" roles="manager-gui,manager-script,manager-jmx,manager-status"/>

  /root/svr/tomcat/webapps/manager/META-INF/context.xml 注释掉value
```

 // 访问tomcat的manager提示403
https://blog.csdn.net/wifi74262580/article/details/81743725

> 在jenkins中选择tomcat版本，并添加用户名和密码

```java
 vim server.xml
 修改端口号，避免重复 // 8030
```

> 在jenkins中添加tomcat url：http://192.168.109.122:8030

```java
  cd /root/.jenkins/workspace/zhanyun/target

```

> 在jenkins中war/ear files： target/*.war
> 
> context path:zhanyun

> 然后应用、保存。启动tomcat。

![warTotomcat.png](./images/warTotomcat.png)

https://blog.csdn.net/eguid_1/article/details/52609600

并且Springbot 最好使用8以上版本的tomcat

