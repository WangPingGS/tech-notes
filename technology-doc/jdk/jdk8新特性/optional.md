# Optional类

​	Optional<T> 类(java.util.Optional) 是一个容器类，代表一个值存在或不存在，原来用null 表示一个值不存在，现在Optional 可以更好的表达这个概念。并且可以避免空指针异常。

常用方法：

- Optional.of(T t) : 创建一个Optional 实例，t为null时，会产生空指针异常
- Optional.empty() : 创建一个空的Optional 实例
- Optional.ofNullable(T t):若t 不为null,创建Optional 实例,否则创建空实例
- isPresent() : 判断是否包含值
- orElse(T t) : 如果调用对象包含值，返回该值，否则返回t
- orElseGet(Supplier s) :如果调用对象包含值，返回该值，否则返回s 获取的值
- map(Function f): 如果有值对其处理，并返回处理后的Optional，否则返回Optional.empty()
- flatMap(Function mapper):与map 类似，要求返回值必须是Optional



```java
@Test
public void test() {
    Optional<Employee> op = Optional.of(new Employee());
    // Optional<Employee> op = Optional.of(null); // 会产生空指针异常
    Employee employee = op.get();
    System.out.println(employee);
}

// 创建一个空的Optional实例
@Test
public void test1() {
    Optional<Employee> op = Optional.empty();
    System.out.println(op.get());
}

@Test
public void test2() {
    Optional<Employee> op = Optional.ofNullable(null);
    // Optional<Employee> op = Optional.ofNullable(null);
    if (op.isPresent()) {
        System.out.println(op.get());
    }
    // System.out.println(op.orElse(new Employee("a", 15, 100.00, Employee.Status.BUSY)));
    System.out.println(op.orElseGet(Employee::new));
}

@Test
public void test3() {
    Optional<Employee> op = Optional.ofNullable(null);
    Optional<String> optionalS = op.map(Employee::getName);
    System.out.println(optionalS.get());
}

@Test
public void test4() {
    Man man = new Man();
    System.out.println(getGodnessName(man));
}

private String getGodnessName(Man man) {
    if (null != man) {
        Godness godness = man.getGodness();
        if (null != godness) {
            return godness.getName();
        }
    }
    return "小花";
}


@Test
public void test5() {
    Optional<Godness> godness = Optional.ofNullable(new Godness("haha"));
    NewMan newMan = new NewMan();
    newMan.setGodness(godness);
    Optional<NewMan> man = Optional.ofNullable(newMan);
    String str = getGodnessName2(man);
    System.out.println(str);
}

private String getGodnessName2(Optional<NewMan> man) {
    return man.orElse(new NewMan())
            .getGodness()
            .orElse(new Godness("小花"))
            .getName();
}

```