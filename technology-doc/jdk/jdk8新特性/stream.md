# Stream API

- Stream 是Java8 中处理集合的抽象概念，可以将集合转换成stream，然后执行非常复杂的查找、过滤和映射数据等操作；

- 使用Stream API 对集合数据进行操作，就类似于使用SQL 执行的数据库查询。
- 也可以使用Stream API 来并行执行操作。

简而言之，Stream API 提供了一种高效且易于使用的处理数据的方式。



## 1. Stream

流是数据渠道，用于操作数据源（集合、数组）所生成的元素序列。

- 集合讲的的是数据，流将的是计算；
- Stream自己不会存储元素；
- Stream不会改变源对象，相反会返回一个持有结果的新Stream；
- Stream操作是延迟执行的，他们会等到需要结果的时候才执行，比如终止操作执行时 处理才会执行。

![](./images/20190410160944.jpg)

## 2. Stream操作的三个步骤

- 创建Stream
  - 一个数据源（集合、数组）中获取流；
- 中间操作
  - 一个中间的操作链，对流中的数据进行操作；
- 终止操作
  - 执行中间操作链，并产生结果。

![1554884189859](./images\1554884189859.png)

### 2.1 创建Stream

- 由集合创建流，Java8 中的Collection 接口被扩展，提供了两个获取流的方法
  - default Stream<E> stream() : 返回一个顺序流
  - default Stream<E> parallelStream() : 返回一个并行流
- 由数组创建流，Java8 中的Arrays 的静态方法stream() 可以获取数组流，重载形式，能够处理对应基本类型的数组，有int、long、double
  - static <T> Stream<T> stream(T[] array): 返回一个流
- 由值创建流，使用静态方法Stream.of(), 通过显示值创建一个流。它可以接收任意数量的参数
  - public static<T> Stream<T> of(T... values) : 返回一个流
- 创建无限流，使用静态方法Stream.iterate() 和Stream.generate(), 创建无限流
  - public static<T> Stream<T> iterate(final T seed, final UnaryOperator<T> f)
  - public static<T> Stream<T> generate(Supplier<T> s)

#### 示例

```java
/**
 * 创建stream
 */
@Test
public void test1() {
    // (1).可以通过Collection系列集合提供的stream() 或 parallelStream()
    List<String> list = new ArrayList<>();
    Stream<String> stream = list.stream();
    // (2).通过Arrays中静态方法stream() 获取数组流
    Employee[] employees = new Employee[10];
    Stream<Employee> stream1 = Arrays.stream(employees);
    // (3).通过Stream类中的静态方法of()
    Stream<String> stream2 = Stream.of("a", "v", "c");
    // (4).创建无限流
    // 迭代
    Stream<Integer> stream3 = Stream.iterate(0, (x) -> x + 2);
    stream3.limit(10).forEach(System.out::println);
    // 生成
    Stream<Double> stream4 = Stream.generate(Math::random);
    stream4.limit(5).forEach(System.out::println);
}
```



### 2.2 中间操作

​	多个中间操作可以连接起来形成一个流水线，除非流水线上触发终止操作，否则中间操作不会执行任何的处理！而在终止操作时一次性全部处理，称为“惰性求值”。

#### 2.2.1 筛选与切片

- filter-接收Lambda，从流中排除某些元素
- limit-截断流，使其元素不超过指定个数，找到limit要求的元素时，将停止迭代，提高效率
- skip(n)-跳过元素，返回一个扔掉前n个元素的流。若流中的元素不足n个，则返回一个空流。与limit(n)互补
- distinct-筛选，通过流所生成元素的hashcode和equals方法去重元素

##### 示例

```java
/**
 * filter
 */
@Test
public void test3() {
    // 中间操作，不会执行任何操作。包括处理流操作。只有有终止操作时，才会执行中间操作
    Stream<Employee> stream = employees.stream().filter((e) -> {
        System.out.println("Stream api 的中间操作");
        return e.getAge() > 35;
    });
    // 终止操作：一次性执行全部过程，即“惰性求值”
    stream.forEach(System.out::println);
}

/**
 * limit-截断流，使其元素不超过指定个数
 * 找到limit要求的元素时，将停止迭代，提高效率
 */
@Test
public void test4() {
    employees.stream().filter((e) -> {
        System.out.println("短路"); // && ||
        return e.getSalary() > 5000;})
            .limit(2)
            .forEach(System.out::println);
}

// skip(n)
@Test
public void test() {
    employees.stream().filter((e) -> e.getSalary() > 5000)
            .skip(2)
            .distinct() // distinct-筛选
            .forEach(System.out::println);
}
```



#### 2.2.2 映射

- map-接收Lambda，将元素转换成其他形式或提取信息。接收一个函数型接口作为参数，该函数会被应用到每个元素上，并将其映射成一个新的元素
- flatMap-接收一个函数作为参数，将流中的每一个值都换成另一个流，然后把所有流连接成一个流。避免了map中形成的字流
- map-flatMap类似于add-addAll

##### 示例

```java
/**
 * map
 */
@Test
public void test5() {
    // 将元素转换成其他形式
    List<String> list = Arrays.asList("sadf", "fgs", "abfc");
    list.stream().map(String::toUpperCase)
            .forEach(System.out::println);
    System.out.println("----------------------");
    // 提取信息
    employees.stream().map(Employee::getName)
            .distinct()
            .forEach(System.out::println);
    System.out.println("----------------------");
    Stream<Stream<Character>> stream = list.stream()
            .map(TestStreamApi::filterCharacter); // {{s, a, d, f}, {f, g, s}...}
    stream.forEach((sm) -> sm.forEach(System.out::println)); // {s, a, d, f, f, g, s}
    System.out.println("---------flatMap-------------");
    // flatMap
    // map:把流中的流加入到新流。flatMap将流中的流的元素加入到新流。
    Stream<Character> stream1 = list.stream()
            .flatMap(TestStreamApi::filterCharacter); // {s, a, d, f, f, g, s}
    stream1.forEach(System.out::println);
}

public static Stream<Character> filterCharacter(String str) {
    List<Character> list = new ArrayList<>();
    for (Character c : str.toCharArray()) {
        list.add(c);
    }
    return list.stream();
}

/**
 * map-flatMap类似于add-addAll
 */
@Test
public void test6() {
    List<String> list = Arrays.asList("sadf", "fgs", "abfc");
    List list1 = new ArrayList();
    list1.add(11);
    list1.add(22);
    // list1.add(list); // [11, 22, [sadf, fgs, abfc]]
    list1.addAll(list); // [11, 22, sadf, fgs, abfc]
    System.out.println(list1);
}
```



#### 2.2.3 排序

- sorted-自然排序（实现了Comparable的compareTo方法，为自然排序）
- sorted(Comparator cm)-定制排序



##### 示例

```java
/**
 * 排序
 */
@Test
public void test7() {
    // sorted-自然排序（Comparable）
    List<String> list = Arrays.asList("sadf", "fgs", "abfc");
    list.stream()
            .sorted()
            .forEach(System.out::println);
    // sorted(Comparator cm)-定制排序
    employees.stream()
            .sorted((e1, e2) -> {
                if (e1.getAge().equals(e2.getAge())) {
                    return e1.getName().compareTo(e2.getName());
                } else {
                    return e1.getAge().compareTo(e2.getAge());
                }
            })
            .forEach(System.out::println);
}
```



### 2.3 终止操作

​	终端操作会从流的流水线生成结果。其结果可以是任何不是流的值，例如：List、Integer，甚至是void 。

#### 2.3.1 查找与匹配

- allMatch-检查是否匹配所有元素
- anyMatch-检查是否至少匹配一个元素
- noneMatch-检查是否没有匹配所有元素
- findFirst-返回第一个元素
- findAny-返回当前流中的任意一个元素
- count-返回流中元素的总数
- max-返回流中的最大值
- min-返回流中的最小值
- forEach(Consumerc)-内部迭代(使用Collection 接口需要用户去做迭代，称为外部迭代。相反，Stream API 使用内部迭代——它帮你把迭代做了)

##### 示例

```java
/**
 * 查找与匹配
 */
@Test
public void test8() {
    // allMatch-检查是否匹配所有元素
    boolean fl = employees.stream()
            .allMatch((e) -> e.getStatus().equals(Employee.Status.BUSY));
    System.out.println(fl);
    // anyMatch-检查是否至少匹配一个元素
    boolean f2 = employees.stream()
            .anyMatch((e) -> e.getStatus().equals(Employee.Status.BUSY));
    System.out.println(f2);
    // noneMatch-检查是否没有匹配所有元素
    boolean f3 = employees.stream()
            .noneMatch((e) -> e.getStatus().equals(Employee.Status.BUSY));
    System.out.println(f3);
    // findFirst-返回第一个元素
    Optional<Employee> employee = employees.stream()
            .sorted((e1, e2) -> Double.compare(e1.getSalary(), e2.getSalary()))
            .findFirst();
    System.out.println(employee.get());
    // findAny-返回当前流中的任意一个元素
    Optional<Employee> employee1 = employees.stream()
            .filter((e) -> e.getStatus().equals(Employee.Status.FREE))
            .findAny();
    System.out.println(employee1.get());
    // count-返回流中元素的总数
    System.out.println(employees.stream().count());
    // max-返回流中的最大值
    System.out.println(employees.stream().max((e1, e2) -> Double.compare(e1.getSalary(), e2.getSalary())));
    // min-返回流中的最小值,获取工资最小的是多少
    Optional<Double> money = employees.stream()
            .map(Employee::getSalary)
            .min(Double::compareTo);
    System.out.println(money.get());
}
```



#### 2.3.2 归约

- reduce(T identify, BinaryOperator)/reduce(BinaryOperator)-可以将流中元素反复结合起来，得到一个值
  - 第一个参数是上一次reduce处理的结果
  - 第二个参数是流中要处理的下一个元素
- map 和reduce 的连接通常称为map-reduce 模式，因Google 用它来进行网络搜索而出名

##### 示例

```java
/**
 * reduce
 */
@Test
public void test9() {
    List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
    int sum = list.stream().reduce(0, (x, y) -> x + y); // 0 为起始值
    System.out.println(sum);
    System.out.println("---------工资总和-----------");
    double sumMoney = employees.stream()
            .map(Employee::getSalary)
            .reduce(0.0, (x, y) -> x + y);
    System.out.println(sumMoney);
}
```



#### 2.3.3 收集

Collector 接口中方法的实现决定了如何对流执行收集操作(如收集到List、Set、Map)。但是Collectors 实用类提供了很多静态方法，可以方便地创建常见收集器实例。

- collect-将流转换成其他形式。接收一个Collector接口的实现，用于给Stream中元素做汇总的方法

##### 示例

```java
@Test
public void test10() {
    List<String> list = employees.stream()
            .map(Employee::getName)
            .collect(Collectors.toList());
    list.forEach(System.out::println);

    System.out.println("----------set-------------");
    Set<String> set = employees.stream()
            .map(Employee::getName)
            .collect(Collectors.toSet());
    set.forEach(System.out::println);

    // 总数
    Long count = employees.stream()
            .collect(Collectors.counting());
    System.out.println(count);
    // 平均值
    double avg = employees.stream()
            .collect(Collectors.averagingDouble(Employee::getSalary));
    System.out.println(avg);
    // 总和
    Double sum = employees.stream()
            .collect(Collectors.summingDouble(Employee::getSalary));
    System.out.println(sum);
    // 最大值
    Optional<Employee> max = employees.stream()
            .collect(Collectors.maxBy((e1, e2) -> Double.compare(e1.getSalary(), e2.getSalary())));
    System.out.println(max.get());

    // 分组
    Map<Employee.Status, List<Employee>> map = employees.stream()
            .collect(Collectors.groupingBy(Employee::getStatus));
    System.out.println(map);

    // 多级分组
    Map<Employee.Status, Map<Object, List<Employee>>> m = employees.stream()
            .collect(Collectors.groupingBy(Employee::getStatus, Collectors.groupingBy((e) -> {
                if (((Employee) e).getAge() <= 35) {
                    return "青年";
                } else if (((Employee) e).getAge() <= 50) {
                    return "中年";
                } else {
                    return "老年";
                }
            })));
    System.out.println(m);
    // 分区
    Map<Boolean, List<Employee>> map1 = employees.stream()
            .collect(Collectors.partitioningBy((e) -> e.getSalary() > 8000));
    System.out.println(map1);

    // 汇总
    DoubleSummaryStatistics dss = employees.stream()
            .collect(Collectors.summarizingDouble(Employee::getSalary));
    System.out.println(dss.getMax());
    System.out.println(dss.getAverage());

    // join
    String str = employees.stream()
            .map(Employee::getName)
            .collect(Collectors.joining(",", "===", "==="));
    System.out.println(str);
}
```



## 3. 串行流和并行流

​	并行流就是把一个内容分成多个数据块，并用不同的线程分别处理每个数据块的流。Java 8 中将并行进行了优化，我们可以很容易的对数据进行并行操作。Stream API 可以声明性地通过parallel() 与sequential() 在并行流与顺序流之间进行切换。

多线程提高cpu的利用率



### 3.1 了解Fork/Join框架

​	Fork/Join框架就是在必要的情况下，将一个大任务，进行拆分(fork)成若干个小任务（拆到不可再拆时），再将一个个的小任务运算的结果进行join 汇总。

- 任务递归分配成若干小任务
- 并行求值
- 部分结果进行合并

![1554888150825](./images\1554888150825.png) 

### 3.2 采用“工作窃取”模式（work-stealing）

​	当执行新的任务时它可以将其拆分分成更小的任务执行，并将小任务加到线程队列中，然后再从一个随机线程的队列中偷一个并把它放在自己的队列中。

​	相对于一般的线程池实现,fork/join框架的优势体现在对其中包含的任务的处理方式上.在一般的线程池中,如果一个线程正在执行的任务由于某些原因无法继续运行,那么该线程会处于等待状态.而在fork/join框架实现中,如果某个子问题由于等待另外一个子问题的完成而无法继续运行.那么处理该子问题的线程会主动寻找其他尚未运行的子问题来执行.这种方式减少了线程的等待时间,提高了性能.



### 3.3 示例

```java
@Test
public void test11() {
    Instant start = Instant.now();
    Long l = LongStream.rangeClosed(0, 10000000000L)
            .parallel()
            .reduce(0, Long::sum);
    System.out.println(l);
    Instant end = Instant.now();
    System.out.println("耗费时间为：" + Duration.between(start, end).toMillis());
}
```