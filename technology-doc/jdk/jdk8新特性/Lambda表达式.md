# Lambda表达式

​	Lambda是一个匿名函数，可以把lambda表达式理解为一段可以传递的代码。可以写出更简洁、灵活的代码，代码风格更紧凑，使得java语言能力上有所提升。

## 1、Lambda表达式的基础语法

​	Java8中引入了一个新的操作符 “->” ，该操作符称为箭头操作符或Lambda 操作符。箭头操作符将Lambda表达式拆分成两部分。

- 左侧： Lambda表达式的参数列表，对应接口抽象方法的参数列表；
- 右侧： 表达式中所需执行的功能，即Lambda体，对应抽象方法的实现。

```java
*    语法格式一： 无参数，无返回值
*      () -> System.out.println("hello lambda");
*    语法格式二： 一个参数，无返回值
*      (x) -> System.out.println(x);
*    语法格式三： 只有一个参数，参数的小括号可以省略
*      x -> System.out.println(x);
*    语法格式四：有多个参数，并且lamda体中有多条语句，有返回值，lambda体中需要大括号
*         Comparator<Integer> comparator = (x, y) -> {
*             System.out.println("函数式接口");
*             return Integer.compare(x, y);
*         };
*     语法格式五：lambda体中只有一条语句，return和大括号都可以省略
*     语法格式六：参数数据类型可以省略，jvm的编译器通过目标上下文推断出数据类型，即“类型推断”
*     左右遇一括号省
*     左侧推断类型省
```

​	注：Lambda表达式需要接口的支持，接口只有一个抽象方法才能使用Lambda表达式。

## 2、Lambda表达式需要“函数式接口”的支持

​	函数式接口：接口中只有一个抽象方法的接口，称为函数式接口。可以使用注解@FunctionalInterface 修饰接口。可以检查是否是函数式接口。（被修饰的接口中只有一个抽象方法，否则报错）



## 3、java内置的四大核心函数式接口

- ConSumer<T>：消费型接口
  - void accept(T t);
- Supplier<T>: 供给型接口
  - T get();
- Function<T, R> 函数型接口
  - R apply(T t);
- Predicate<T> 断言型接口
  - Boolean test(T t);

## 4、示例

### 4.1 Lambda表达式语法示例

```java
/**
 * 原来的匿名内部类
 */
@Test
public void test1() {
    Comparator<Integer> comparator = new Comparator<Integer>() {
        @Override
        public int compare(Integer o1, Integer o2) {
            return Integer.compare(o1, o2);
        }
    };
    TreeSet<Integer> set = new TreeSet<>(comparator);
}

/**
 * Lambda表达式
 */
@Test
public void test2() {
    Comparator<Integer> comparator = (o1, o2) -> Integer.compare(o1, o2);
    TreeSet<Integer> set = new TreeSet<>(comparator);
}
```



```java
/**
 * 语法格式一： 无参数，无返回值
 */
@Test
public void test1() {
    int num = 0; // jdk1.7以前，必须是final。jdk1.8后，默认会添加final
    Runnable r = new Runnable() {
        @Override
        public void run() {
            System.out.println("hello lambda" + num);
        }
    };
    r.run();
    Runnable r1 = () -> System.out.println("hello lambda");
    r1.run();
}
```



```java
/**
 * 语法格式二： 一个参数，无返回值
 */
@Test
public void test2() {
    Consumer<String> consumer = (x) -> System.out.println(x);
    consumer.accept("hello lambda");
}
```



```java
/**
 * 语法格式四：有多个参数，并且lamda体中有多条语句，有返回值
 */
public void test3() {
    Comparator<Integer> comparator = (x, y) -> {
        System.out.println("函数式接口");
        return Integer.compare(x, y);
    };
}
```



```java
/**
     * 语法格式六：参数数据类型可以省略，jvm的编译器通过上下文推断出数据类型，即“类型推断”
     */
    public void test4() {
        String str[] = {"sa", "saasa"};
//        String s2[];
//        s2 =  {"sa", "saasa"};
        List<String> list = new ArrayList<>();
        show(new HashMap<>());
    }
private void show(Map<String, Integer> map) {

}
```

### 4.2 Lambda使用示例

A. 集合中的员工排序，首先根据年龄排序，年龄相同根据姓名排序

```java
List<Employee> employees = Arrays.asList(
        new Employee("张三", 18, 9999.99),
        new Employee("李四", 38, 5555.99),
        new Employee("王五", 50, 6666.99),
        new Employee("赵6", 16, 3333.99),
        new Employee("田7", 8, 7777.99)
);

@Test
public void test6() {
    Collections.sort(employees, (x, y) -> {
        if (x.getAge() == y.getAge()) {
            return x.getName().compareTo(y.getName());
        } else {
            return -Integer.compare(x.getAge(), y.getAge());
        }
    });
    for (Employee employee : employees) {
        System.out.println(employee);
    }
}
```

B. 对两个数运算，并返回结果

```java
@Test
public void test8() {
    System.out.println("10 * 9 = " + operate(10L, 9L, (x, y) -> x * y));
    System.out.println("10 - 9 = " + operate(10L, 9L, (x, y) -> x - y));
}

public Long operate(Long x, Long y, MyFuture<Long, Long> myFuture) {
    return myFuture.deal(x, y);
}
```

### 4.3 内置的函数式接口示例

```java
/* 三、java内置的四大核心函数式接口*/
 // Consumer<T> 消费型接口

 @Test
 public void testConsumer() {
     happy(1000, (m) -> System.out.println("唱歌。。。。。。。消费" + m + "元"));
     happy(2000, (m) -> System.out.println("唱歌。。。。。。。消费" + m + "元"));
 }

 private void happy(double money, Consumer<Double> consumer) {
     consumer.accept(money);
 }
```



```java
// Supplier<T> 供给型接口
// 产生指定个数的整数，并放入集合中
@Test
public void testSupplier() {
    List<Integer> list = getNumList(10, () -> (int) (Math.random() * 100));
    for (Integer i : list) {
        System.out.println(i);
    }
    System.out.println("------------------------");
    List<Integer> list1 = getNumList(2, () -> (int)(Math.random() *100));
    for (Integer i : list1) {
        System.out.println(i);
    }
}
private List<Integer> getNumList(int num, Supplier<Integer> supplier) {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i< num; i++) {
            list.add(supplier.get());
        }
        return list;
}
```



```java
// Function<T, R> 函数型接口
// 用于处理字符串


@Test
public void testFunction() {
    System.out.println(dealStr("afsalfjs", (x) -> x.toUpperCase()));
    System.out.println(dealStr("AFSALFJS", (x) -> x.toLowerCase()));
}

public String dealStr(String str, Function<String, String> function) {
    return function.apply(str);
}
```



```java
// Predicate<T t> 断言型接口
// 将满足条件的字符串添加集合中
@Test
public void testPredicate() {
    String[] str = {"ass", "dssa", "sdsdf", "cvv"};
    List<String> stringList = Arrays.asList(str);
    stringList = filterStr(stringList, (x) -> x.contains("a"));
    for (String s : stringList) {
        System.out.println(s);
    }
}

public List<String> filterStr(List<String> list, Predicate<String> predicate) {
    List<String> strings = new ArrayList<>();
    for (String s : list) {
        if (predicate.test(s)) {
            strings.add(s);
        }
    }
    return strings;
}
```

## 5、Lambda表达式的好处

简化代码。比如从集合中获取年龄大于35的员工信息会定义一个方法，获取薪资高于5000的员工信息也会定义一个方法，这样代码复用性差，代码繁多。使用以下的优化方式

```java
List<Employee> employees = Arrays.asList(
            new Employee("张三", 18, 9999.99),
            new Employee("李四", 38, 5555.99),
            new Employee("王五", 50, 6666.99),
            new Employee("赵6", 16, 3333.99),
            new Employee("田7", 8, 7777.99)
    );

    // 需求：获取当前公司中员工年龄大于35的员工信息
    @Test
    public void test3() {
        List<Employee> list = filterEmployees(employees);
        for (Employee employee : list) {
            System.out.println(employee);
        }
    }
    public List<Employee> filterEmployees(List<Employee> list) {
        List<Employee> employees = new ArrayList<>();
        for (Employee employee : list) {
            if (employee.getAge() > 35) {
                employees.add(employee);
            }
        }
        return employees;
    }

    // 需求：获取当前公司员工工资大于5000的员工信息
    public List<Employee> filterEmployee2(List<Employee> list) {
        List<Employee> employees = new ArrayList<>();
        for (Employee employee : list) {
            if (employee.getSalary() > 5000) {
                employees.add(employee);
            }
        }
        return employees;
    }
```



#### 优化方式一：策略设计模式

FilterEmployeeByAge实现了MyPredicate接口，并实现对应的策略功能方法

```java
@Test
public void test4() {
    List<Employee> list = filterEmployee(employees, new FilterEmployeeByAge());
    for (Employee employee : list) {
        System.out.println(employee);
    }
    System.out.println("------------------------------");
}
public List<Employee> filterEmployee(List<Employee> list, MyPredicate<Employee> myPredicate) {
        List<Employee> employees = new ArrayList<>();
        for (Employee employee : list) {
            if (myPredicate.test(employee)) {
                employees.add(employee);
            }
        }
        return employees;
}
```

#### 优化方式二：匿名内部类或lambda

```java
// 优化方式二：匿名内部类或lambda
@Test
public void test5() {
    List<Employee> list = filterEmployee(employees, (Employee e) -> e.getAge() > 35);
    for (Employee employee : list) {
        System.out.println(employee);
    }
    System.out.println("------------------------------");
    list = filterEmployee(employees, (Employee e) -> e.getSalary() > 5000);
    for (Employee employee : list) {
        System.out.println(employee);
    }
}
```

#### 优化方式三：Stream API

```java
// 优化方式三
@Test
public void test7() {
    employees.stream().filter((employee -> employee.getSalary() > 5000)).limit(2).forEach(System.out::println);
    System.out.println("------------------------------");
    employees.stream().map(Employee::getName).forEach(System.out::println);
}
```



代码详情见：<https://gitlab.com/WangPingGS/tech/tree/master/jdk/src>