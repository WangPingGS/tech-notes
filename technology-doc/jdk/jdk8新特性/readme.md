# Java8新特性

2014年3月发布

- 速度快
- 代码更少
- 强大的Stream API
- 便于并行
- 最大化减少空指针异常 Optional



主要内容

- Lambda表达式
- 函数式接口
- 方法引用与构造器引用
- Stream API
- 接口中的默认方法与静态方法
- 新时间日期API
- 其他新特性

其中最核心的是Lambda表达式和Stream Api。

代码：<https://gitlab.com/WangPingGS/tech/tree/master/jdk>

