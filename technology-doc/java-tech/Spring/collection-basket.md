# 1、Spring的作用

  * 开发当中必然遇到一个类中会去调用另外一个类的对象，之前做法是，直接从类中new另外一个对象，然后去调用该对象的方法。这样会导致调用者和被调用对象的创建过程是高耦合的。

  * 如果被调用的类暂时不用，而用新建的实现类（该类功能上更加强大，但之前的类有可能有用，所以不能删除），那么此时需要修改调用者中new该对象的方式(换成新类)，这样会大量改动调用类，这是完全没有必要的。
    
  * 重点场景比如，web开发中的三层结构开发模式，层与层之间通过相互依赖，通过创建对象的方式，相互调用。

>那么如何解决调用者和被调用对象的创建是高耦合的？

- A.使用工厂模式，把对象的创建交给工厂去完成，调用者只需定义被调用者的接口，然后通过工厂方法去获取需要的对象。

- B.面向接口编程，定义标准和规范

- C.Spring
  - Spring提供工厂类，完成对象的创建；
  - 通过面向接口编程，降低程序中的耦合；
  - 通过依赖注入\反转控制（DI/IOC），自动的给对象属性赋值；
    - DI：依赖注入，由Spring容器主动将bean注入到对象中的属性；
    - IOC：程序中所用的对象由原先的自己创建，变化成交由Spring容器来完成。
      共同的目的是，给对象的属性赋值。
  - aop面向切面编程，抽取核心公共的功能，与真正的业务解耦。



# 2、Spring注入的的三种方法



## 2.1 set注入

> 调用类中定义属性的setter方法，xml中用property来建立依赖关系

```xml
<bean id="userService" class="com.learn.impl.UserServiceImpl">
    <property name="userDao" ref="userDao"></property>
</bean>
```

## 2.2 Constructor注入

> 构造注入，调用类中定义构造方法，对象的属性作为构造方法的参数，xml中用constructor-arg

```xml
    <bean id="kafkaTemplate" class="org.springframework.kafka.core.KafkaTemplate">
        <constructor-arg>
            <!-- 创建kafkatemplate需要使用的producerfactory bean -->
            <bean class="org.springframework.kafka.core.DefaultKafkaProducerFactory">
                <constructor-arg>
                    <map>
                        <entry key="bootstrap.servers" value="${kafka.connect.url}"/>
                        <entry key="retries" value="1"/>
                        <entry key="batch.size" value="524288"/>
                        <entry key="linger.ms" value="6000"/>
                        <entry key="request.timeout.ms" value="90000"/>
                        <entry key="buffer.memory" value="67108864"/>
                        <entry key="connections.max.idle.ms" value="2000"/>
                        <entry key="key.serializer"
                               value="org.apache.kafka.common.serialization.StringSerializer"/>
                        <entry key="value.serializer"
                               value="org.apache.kafka.common.serialization.StringSerializer"/>
                    </map>
                </constructor-arg>
            </bean>
        </constructor-arg>
        <constructor-arg name="autoFlush" value="false"/>
        <property name="defaultTopic" value="${kafka.hetu.topic}"/>
    </bean>
    <!-- 通过name可以指定参数和对应的值 -->
```



## 2.3 接口注入

使用xml配置文件中定义的类名，然后通过Class.forName("类名").newInstance(); 去注入。（了解即可，Spring很少用这种方式）



# 3、自动装配autowire

autowire：配置文件中不需要通过setter或构造方法注入的方式给对象属性赋值，而是通过对象属性的名称或类型和Spring容器中bean的id或class类型自动匹配，匹配成功，则自动给属性赋值。


```xml
    <!-- 这里不使用setter的注入方式，而是使用自动装配的方式，通过对象属性名称userDao和容器中的bean的id来自动匹配 -->
    <bean id="userService" class="com.learn.impl.UserServiceImpl" autowire="byName">
        <!-- <property name="userDao" ref="userDao"></property> -->
    </bean>
    <bean id="userDao" class="com.learn.impl.UserDaoImpl">
    </bean>
```

目的减少了配置文件中内容，使得对象之间的依赖关系注入更自动化。

autowire的类型有：
![autowire.png](./images/autowire.png)

    @Autowired是通过byType形式，用来给指定的属性或方法注入所需的外部资源。  
    Spring 2.5 引入了 @Autowired 注解，它可以对类成员变量、setter方法及构造函数进行标注，完成自动装配的工作。AutowiredAnnotationBeanPostProcessor
    
    当不能确定 Spring 容器中一定拥有某个类的 Bean 时，可以在需要自动注入该类 Bean 的地方可以使用 @Autowired(required = false)，这等于告诉 Spring：在找不到匹配 Bean 时也不报错。一般不这样做。
    
    如果 Spring 容器中拥有多个候选 Bean，Spring 容器在启动时也会抛出 BeanCreationException 异常，Spring 容器将无法确定到底要用哪一个 Bean。
    
    通过 @Qualifier("beanId") 注解指定注入 Bean 的id，这样歧义就消除了

下面是@Autowire的用法:

    a.@Autowired在setter方法上
    b.在属性中使用 @Autowired 注解来除去 setter 方法
    c.构造函数中的 @Autowired 



# 4、常见属性类型的注入

## 4.1 String或基本类型的对应的包装类型

```xml
    <property name="userId" value="1001"></property>
    <property name="userName" value="小平"></property>
```

## 4.2 list集合属性赋值

```xml
    <property name="myList">
        <list>
            <value>1001</value>     <!-- String或基本类型的对应的包装类型 -->
            <ref local="userDao"/> <!-- 当前配置文件中的bean-->
            <bean class="java.util.Date"></bean> <!-- 直接写入bean描述 -->
        </list>
    </property>
```

## 4.3 set集合属性赋值

```xml
    <property name="mySet">
        <set>
            <value>1001</value>     <!-- String或基本类型的对应的包装类型 -->
            <ref local="userDao"/> <!-- 当前配置文件中的bean-->
            <bean class="java.util.Date"></bean> <!-- 直接写入bean描述 -->            
        </set>
    </property>
```

## 4.4 map集合属性赋值

```xml
    <property>
        <map>
            <entry key="userId" value="1001"></entry>
            <entry>
                <key><value>k1</value></key>
                <value>v1<value>
            </entry>
            <entry>
                <key><value>k2</value></key>
                <ref local="userDao" />
            </entry>            
        </map>
    </property>
```

## 4.6 properties的注入

```xml
    <property>
        <props>
            <prop key="k1">v1</prop>
            <prop key="k2">v2</prop>
            <prop key="k3">v3</prop>
        </props>
    </property>
```


# 5、bean的单例和多例

  * singleton（单例）：
    * 对象在整个系统中只有一份，所有的请求都用一个对象来处理，如service和dao层的对象一般是单例的。springmvc中controller默认是单例的，线程不安全，但是有规范，避免线程不安全情况发生。
    * 为什么使用单例：因为没有必要每个请求都新建一个对象的时候，浪费CPU和内存。

  * prototype（多例）
    * 对象在整个系统中可以有多个实例，每个请求用一个新的对象来处理，如struts2中的action，线程安全的，因为多例。
    * 为什么使用多例：防止并发问题，多个请求共享同一个成员变量，应该是每个请求使用自己的成员变量。
    Spring bean 默认是单例模式。


    SpringMVC 是基于单例模式的，所以如果有类的实例全局变量，这个会出现问题。但是根据SpringMVC的设计要求，不推荐存在全局变量，出现的类引用也不过是Service层中的对象，这个对象不具备保存请求数据信息的功能，如果有保存信息也直接传入了函数内部。函数内部定义的变量时线程安全的。所以应该是可以放心使用的。
    
    SpringMVC和Sturts2设计的不同是请求的信息是否保存在Controller层中。Struts2的Action是需要保存请求信息的。而SpringMVC是不保存的。所以Struts2是用prototype，SpringMVC是singleton。
    
    整体来说，SpringMVC的Controller是线程不安全的，但是对设计有规定要求，这样可以避免线程安全问题。不只是Controller，包括Service和Dao都是不安全的。



# 6、Spring容器bean对象的懒加载

  * 如果是单例模式的情况下：不存在对象的懒加载，spring容器加载完成，立马创建对象。
  * 如果是多例模式的情况下：spring容器加载完成后，不会立马创建bean的对象，只有ac.getBean的时候，才会创建对象。

> xml配置方式通过default-lazy-init 和 lazy-init 属性，前者统一指定，后者在bean定义时指定。

```xml
<!-- default-lazy-init统一指定 -->
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-2.0.xsd"
       default-lazy-init="true">
</beans>

<!-- bean定义时指定 -->
<bean id="userService" class="com.learn.service.impl.UserServiceImpl"></bean>
```
> SpribgBoot方式测试懒加载

    这里使用javaConfig方式配置。通过@Configuration注解，spring会自动识别并加载，与xml配置功能一致。@lazy可以直接用在@bean或@Component定义的类上，也可以使用在 @Configuration定义类上，后者表示该类中所有@Bean方法对应类被懒加载，与default-lazy-init 功能一致。

```java
@Configuration
public class SysConfig {

    @Bean(initMethod = "init",destroyMethod = "destroy")
    @Lazy
    public ConfigBean configBean(){
        return new ConfigBean("A");
    }
}
```

```java
@RunWith(SpringRunner.class)
@SpringBootTest
public class ConfigBeanTest {

    private Logger logger = LoggerFactory.getLogger(ConfigBeanTest.class);

    @Autowired
    ApplicationContext context;

    ConfigBean configBean;

    @Test
    public void lazyTest() {
        logger.info("bean lazy load...............");
        logger.info("configBean :{}",context.getBean("configBean"));
    }
}
```



# 7、Spring容器中bean对象的生命周期

通过bean标签的属性：

- init-method，指定对象的初始化调用方法；
- destory-method，指定对象的销毁调用方法。

实例化、初始化、提供服务、销毁（容器停止时，只对单例有效）。

要想Spring容器控制bean的生命周期，那么bean必须是单例。多例的bean中的资源，需要程序员自己关闭。



# 8、Spring的另一个特性AOP编程

## 8.1 AOP中几个概念

在执行dao方法时，添加日志打印作为增强的公共功能，以此场景来理解aop的相关概念。

- Aop：面向切面编程，是对面向对象编程的补充。将系统中将增强的公共的功能进行分离单独实现，在程序运行的过程中将增强的公共的功能和核心的业务共进行整合(通过产生核心的业务对象的代理对象实现)----实现原理：动态代理 ；
- ASPECT：切面，封装共性的增强公共功能的类；如Log类，类中实现了advice通知方式；
- Joinpoint：连接点，表示一个方法的执行，当前一个正在执行的方法，完成增强功能整合；
- Pointcut：切入点，表示按照规则找到需要处理的业务类；<aop: pointcut express='' id='pc'>表示作用于哪些包所有类或某个类。
- Advice：通知，表示切面中封装的增强功能(方法)，通过在核心业务对象方法around，before，after位置完成执行；
  - 前置通知（Before advice）：在某连接点之前执行的通知，但这个通知不能阻止连接点之前的执行流程（除非它抛出一个异常）。 
  -  后置通知（After returning advice）：在某连接点正常完成后执行的通知：例如，一个方法没有抛出任何异常，正常返回。 
  - 异常通知（After throwing advice）：在方法抛出异常退出时执行的通知。 
  -  最终通知（After (finally) advice）：当某连接点退出的时候执行的通知（不论是正常返回还是异常退出）。 
  - 环绕通知（Around Advice）：包围一个连接点的通知，如方法调用。这是最强大的一种通知类型。环绕通知可以在方法调用前后完成自定义的行为。它也会选择是否继续执行连接点或直接返回它自己的返回值或抛出异常来结束执行。
- Weaving：织入，将核心业务功能和增强的功能(advice)，进行整和的过程叫织入(程序运行的时候完成)。
  - <aop: advisor advice-ref='log' point-cut='pc'>  将核心业务功能pc和增强的公共功能log进行整合。



## 8.2 spring中aop的具体配置



## 8.3 Spring aop的几种实现方式

### 8.3.1 定义切面需要实现特定接口

- 前置通知：MethodBeforeAdvice
- 后置通知：AfterReturningAdvice
- 环绕通知：MethodInterceptor
- 异常通知：ThrowsAdvice
- 最终通知



### 8.3.2 通过注解实现

目的：减少xml中的配置内容；条件是：

- 开启了扫描注解；

- 开启了aspect注解。



### 8.2.3 基于xml配置，切面类不需要实现特定接口

通知advice在xml配置中指定。



## 8.4 Spring Aop常见的应用场景

- 声明式事务处理
  - 编程式事务处理：将事务处理的功能，直接嵌入到业务逻辑代码中。开启事务，执行sql操作，提交事务。（回滚事务）
  - 而声明式事务处理：将事务处理的功能和业务逻辑分离，在执行的时候又按照编程式事务处理的顺序执行。
  - cglib动态代理实现
    - Enhancer en = new Enhance();
    - en.setSuperClass();
    - en.setCallback(new MethodInterceptor(){});
    - en.create();  创建代理对象
- 日志打印



### 8.4.1 Spring的声明式事务的实现

- 实例化sessionFactory的bean，会加载orm框架的配置文件；
- 实例化事务管理器bean，TransactionManager；
- 声明式事务通知advice，会在拦截哪些方法，也就是哪些方法会加入事务管理；相当于切面类的实现。
- 进行aop配置
  - 声明切入点pointcut
  - 织入，整合切面类（公共的功能）和切入点（作用域哪些包下的哪些类）。



# 9、注解掌握、Spring中的注解以及开发中用到的注解整理

> 对象属性赋值

  * @Autowired：对象属性赋值，默认按照byType方式进行bean匹配，是javaEE的注解；存在多个实例可以结合@Qualifier注解一起使用。
  * @Resource：对象属性赋值，默认按照byName方式进行bean匹配，当找不到与名称匹配的bean才会按byType装配。是Spring提供的注解；使用@Resource可以减少代码和Spring之间的耦合。使用时可指定name。

> 将类纳入Spring容器

  * @Controller：用于标注控制层组件（如struts中的action）。
  * @Service：用于标注业务层组件。
  * @Repository：用于标注数据访问组件。
  * @Scope("prototype")：指定bean是单例（singleton）或多例（prototype）
  * @Component：泛指组件，当组件不好归类的时候，我们可以使用这个注解进行标注。
  * @Configuration把一个类作为一个IoC容器，它的某个方法头上如果注册了@Bean，就会作为这个Spring容器中的Bean。
  * @Lazy(true) 表示延迟加载。
  * @PostConstruct用于指定初始化方法（用在方法上）
  * @PreDestory用于指定销毁方法（用在方法上）
  * @DependsOn：定义Bean初始化及销毁时的顺序
  * @Primary：自动装配时当出现多个Bean候选者时，被注解为@Primary的Bean将作为首选者，否则将抛出异常
  * @Async异步方法调用
