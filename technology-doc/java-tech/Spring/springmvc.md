# 1、什么是SpringMvc以及原理

Springmvc:是一个web层mvc框架，接受请求，处理请求，做出响应。由以下几部分组成：

- 核心控制器：DispatcherServlet，在web.xml中配置
- 映射器：从url到处理器方法的映射，tomcat启动后核心控制器会加载并存到LinkedHashMap中；
  - 非注解的映射器（通过beanName进行url的映射）；
  - 注解的方式，通过@RequestMapping 给handler中的方法指定url。
- 处理器：进行请求处理，并返回响应数据和页面信息（ModelAndView对象）。
  - 实现Controller接口，只有一个方法，返回ModelAndView对象；
  - 实现HttpRequestHandler接口，只有一个方法，返回null；
  - @Controller，自定义多个方法，返回值有String、void、ModelAndView等。
- 适配器：调用我们自己写的处理器方法，@Controller的处理器中的方法，会通过反射的形式调用。
  - 为什么用到适配器，因为核心控制器不知道处理器是哪种方式实现的，只需一个适配接口HandlerAdapter即可。这里HandlerAdapter相当于usb头，另一端相当于不同的设备。
  - SimpleControllerHandlerAdapter；
  - HttpRequestHandlerAdapter；
  - RequestMappingHandlerAdapter。
- 视图解析器：解析ModelAndView对象，完成客户端响应。
  - InternalResourceViewResolver，可以配置页面路径的前缀和后缀。

![mvc原理.JPG](./images/mvc原理.JPG)





# 2、 handler中方法的默认参数

- HttpServletRequest

- HttpServletResponse

- HttpSession

- Model：springmvc提供的，相当于Map（键值对），把数据封装到Model中。handler中定义的方法有String返回值。
  - 如果使用请求转发跳转：model中封装的值，相当于在request中传到页面；
  - 如果使用的重定向：相当于完成一个url传参，参数跟在url后面。



# 3、SpringMVC的自定义拦截器

实现HnadlerInterceptor

有三个方法：

- preHandle，处理器方法执行前执行，返回方法为true，执行处理器方法，否则不执行直接返回；
- postHandle，处理器方法执行完之后，结果视图创建之前，执行；
- afterCompletion，结果视图创建之后，执行，我们可以进行清理相关操作。