## spring集成dubbo

环境准备

  * 安装zookeeper作为注册中心 127.0.0.1:2181
  * dubbo-admin
  * dubbo-monitor

> dubbo.properties属性加载顺序

  * JVM 启动 -D 参数优先，这样可以使用户在部署和启动时进行参数重写，比如在启动时需改变协议的端口。
  
  * XML 次之，如果在 XML 中有配置，则 dubbo.properties 中的相应配置项无效。
  
  * Properties 最后，相当于缺省值，只有 XML 没有配置时，dubbo.properties 的相应配置项才会生效，通常用于共享公共配置，比如应用名。dubbo.properties在项目resources目录下。

> 配置启动检查

    项目启动时会检查依赖的服务是否启动、注册中心是否启动，对应的没有启动时报错。
    http://dubbo.apache.org/zh-cn/docs/user/demos/preflight-check.html

  *  关闭某个服务的启动时检查 (没有提供者时报错)：<dubbo:reference interface="com.foo.BarService" check="false" />
  *  关闭所有服务的启动时检查 (没有提供者时报错)：<dubbo:consumer check="false" />
  *  关闭注册中心启动时检查 (注册订阅失败时报错)：<dubbo:registry check="false" />

> 超时配置覆盖关系

    http://dubbo.apache.org/zh-cn/docs/user/configuration/xml.html

  * 方法级优先，接口级次之，全局配置再次之。
  * 如果级别一样，则消费方优先，提供方次之。
  * 其它 retries, loadbalance, actives 等类似。

> 配置重试次数

  * retries="":重试次数，不包含第一次调用，0代表不重试。
  * 幂等（设置重试次数）【查询、删除、修改】、非幂等（不能设置重试次数）【新增】
  * 服务方有多个，那么会轮询访问，直到访问到为止。

> 多版本

    http://dubbo.apache.org/zh-cn/docs/user/demos/multi-versions.html

```xml
	<dubbo:service interface="com.atguigu.gmall.service.UserService" 
		ref="userServiceImpl01" timeout="1000" version="1.0.0">
		<dubbo:method name="getUserAddressList" timeout="1000"></dubbo:method>
	</dubbo:service>
	
	
	<!-- 服务的实现 -->
	<bean id="userServiceImpl01" class="com.atguigu.gmall.service.impl.UserServiceImpl"></bean>
	
	<dubbo:service interface="com.atguigu.gmall.service.UserService" 
		ref="userServiceImpl02" timeout="1000" version="2.0.0">
		<dubbo:method name="getUserAddressList" timeout="1000"></dubbo:method>
	</dubbo:service>
	<bean id="userServiceImpl02" class="com.atguigu.gmall.service.impl.UserServiceImpl2"></bean>
```

> 本地存根

    http://dubbo.apache.org/zh-cn/docs/user/demos/local-stub.html

> dubbo和springBoot整合的三种方式

  * 导入dubbo-starter，在application.properties配置属性，使用@Service【暴露服务】使用@Reference【引用服务】

  * 保留dubbo.xml配置文件;导入dubbo-starter，使用@ImportResource导入dubbo的配置文件即可

    ```java
    // 启动类添加以下注解即可
    @ImportResource(locations="classpath:provider.xml")
    ```

  * 使用注解API的方式：将每一个组件手动创建到容器中,让dubbo来扫描其他的组件

    ```java
    // 启动类添加以下注解即可
    @EnableDubbo(scanBasePackages="com.wp.dubbo")
    ```

```java
@Configuration
public class MyDubboConfig {
	
	@Bean
	public ApplicationConfig applicationConfig() {
		ApplicationConfig applicationConfig = new ApplicationConfig();
		applicationConfig.setName("boot-user-service-provider");
		return applicationConfig;
	}
	
	//<dubbo:registry protocol="zookeeper" address="127.0.0.1:2181"></dubbo:registry>
	@Bean
	public RegistryConfig registryConfig() {
		RegistryConfig registryConfig = new RegistryConfig();
		registryConfig.setProtocol("zookeeper");
		registryConfig.setAddress("127.0.0.1:2181");
		return registryConfig;
	}
	
	//<dubbo:protocol name="dubbo" port="20882"></dubbo:protocol>
	@Bean
	public ProtocolConfig protocolConfig() {
		ProtocolConfig protocolConfig = new ProtocolConfig();
		protocolConfig.setName("dubbo");
		protocolConfig.setPort(20882);
		return protocolConfig;
	}
	
	/**
	 *<dubbo:service interface="com.atguigu.gmall.service.UserService" 
		ref="userServiceImpl01" timeout="1000" version="1.0.0">
		<dubbo:method name="getUserAddressList" timeout="1000"></dubbo:method>
	</dubbo:service>
	 */
	@Bean
	public ServiceConfig<UserService> userServiceConfig(UserService userService){
		ServiceConfig<UserService> serviceConfig = new ServiceConfig<>();
		serviceConfig.setInterface(UserService.class);
		serviceConfig.setRef(userService);
		serviceConfig.setVersion("1.0.0");
		
		//配置每一个method的信息
		MethodConfig methodConfig = new MethodConfig();
		methodConfig.setName("getUserAddressList");
		methodConfig.setTimeout(1000);
		
		//将method的设置关联到service配置中
		List<MethodConfig> methods = new ArrayList<>();
		methods.add(methodConfig);
		serviceConfig.setMethods(methods);
		
		//ProviderConfig
		//MonitorConfig
		
		return serviceConfig;
	}
}    
```

> zookeeper宕机与dubbo直连
```java
// dubbo直连
@Reference(url="127.0.0.1:20882")
```

  zookeeper注册中心宕机，还可以消费dubbo暴露的服务。

* 监控中心宕掉不影响使用，只是丢失部分采样数据
* 数据库宕掉后，注册中心仍能通过缓存提供服务列表查询，但不能注册新服务
* 注册中心对等集群，任意一台宕掉后，将自动切换到另一台
*	注册中心全部宕掉后，服务提供者和服务消费者仍能通过本地缓存通讯
*	服务提供者无状态，任意一台宕掉后，不影响使用
*	服务提供者全部宕掉后，服务消费者应用将无法使用，并无限次重连等待服务提供者恢复

> 集群下dubbo负载均衡配置

默认是随机负载均衡策略

http://dubbo.apache.org/zh-cn/docs/user/demos/loadbalance.html

> 服务降级


> 集群容错


> 整合spring cloud hystrix
