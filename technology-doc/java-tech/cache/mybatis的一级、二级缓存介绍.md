## 1.mybatis的一级、二级缓存介绍

### 1.1 一级缓存

> Mybatis默认开启一级缓存，一级缓存是sqlSession层面的缓存，不同的sqlSession之间是相互隔离的。同一个sqlSession，多次调用同一个mapper和同一个方法的同一个参数，只会查询一次数据库。那么同一个sqlSession是什么场景?（spring集成mybatis的应用中可以想象成每个请求线程获取一个sqlSession，多个线程意味着不同的sqlSession）

```java
public static void main(String[] args) {
        // 自定义的单例SqlSessionFactory模式
        SqlSessionFactory factory = SqlSessionFactoryUtil.openSqlSession();

        // 获得SqlSession对象
        SqlSession sqlSession = factory.openSession();
        // 获得dao实体
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        // 进行两次相同的查询操作
        userMapper.selectByPrimaryKey(1);
        userMapper.selectByPrimaryKey(1);
        // 注意，当我们使用二级缓存时候，sqlSession需要使用commit时候才会生效
        sqlSession.commit();

        System.out.println("\n\n=============================================================");
        // 获得一个新的SqlSession 对象
        SqlSession sqlSession1 = factory.openSession();
        // 进行相同的查询操作
        sqlSession1.getMapper(UserMapper.class).selectByPrimaryKey(1);
        // 注意，当我们使用二级缓存时候，sqlSession需要使用commit时候才会生效
        sqlSession.commit();
}

    // 可以发现，第一次的两个相同操作，只执行了一次数据库。后来的那个操作又进行了数据库查询。
```

### 1.2 二级缓存

> 为了让不同的sqlSession共享数据，那么需要开启二级缓存，默认是关闭的，需要手动配置。

参考： https://www.cnblogs.com/moongeek/p/7689683.html