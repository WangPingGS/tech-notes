## 本地缓存guava

### 1 简单介绍

> Guava Cache是一个全内存的本地缓存实现，它提供了线程安全的实现机制。整体上来说Guava cache 是本地缓存的不二之选，简单易用，性能好。也可以自己通过ConcurrentMap实现本地缓存。
> 
> Cache是和ConcurrentMap很像的东西。最本质的区别的是，cache中的元素可以自动回收，而ConcurrentMap只能专门remove。
> 
> 本地内存缓存需要考虑很多问题，包括并发问题，缓存失效机制，内存不够用时缓存释放，缓存的命中率，缓存的移除等等。 当然这些东西guava都考虑到了。

### 2 适用场景

> * 花费一些内存来提高速度
> * 预期到一些key的值将被不止一次地被查询
> * 缓存的数据不会比内存能够存储的数据多

### 3 缓存加载的两种方式

> * CacheLoader：build方法中可以指定CacheLoader，在缓存不存在时通过CacheLoader的实现自动加载缓存
> 
> * Callable：cache.get(key, new Callable<T>)方式，如果有缓存则返回;否则运算、缓存、然后返回

### 4 案例

#### 4.1 CacheLoader方式

```java
public static void main(String[] args) throws ExecutionException, InterruptedException{
        //缓存接口这里是LoadingCache，LoadingCache在缓存项不存在时可以自动加载缓存
        LoadingCache<Integer,Student> studentCache
                //CacheBuilder的构造函数是私有的，只能通过其静态方法newBuilder()来获得CacheBuilder的实例
                = CacheBuilder.newBuilder()
                //设置并发级别为8，并发级别是指可以同时写缓存的线程数
                .concurrencyLevel(8)
                //设置写缓存后8秒钟过期
                .expireAfterWrite(8, TimeUnit.SECONDS)
                //设置缓存容器的初始容量为10
                .initialCapacity(10)
                //设置缓存最大容量为100，超过100之后就会按照LRU最近虽少使用算法来移除缓存项
                .maximumSize(100)
                //设置要统计缓存的命中率
                .recordStats()
                //设置缓存的移除通知
                .removalListener(new RemovalListener<Object, Object>() {
                    @Override
                    public void onRemoval(RemovalNotification<Object, Object> notification) {
                        System.out.println(notification.getKey() + " was removed, cause is " + notification.getCause());
                    }
                })
                //build方法中可以指定CacheLoader，在缓存不存在时通过CacheLoader的实现自动加载缓存
                .build(
                        new CacheLoader<Integer, Student>() {
                            @Override
                            public Student load(Integer key) throws Exception {
                                System.out.println("load student " + key);
                                Student student = new Student();
                                student.setId(key);
                                student.setName("name " + key);
                                return student;
                            }
                        }
                );

        for (int i=0;i<20;i++) {
            //从缓存中得到数据，由于我们没有设置过缓存，所以需要通过CacheLoader加载缓存数据
            Student student = studentCache.get(1);
            System.out.println(student);
            //休眠1秒
            TimeUnit.SECONDS.sleep(1);
        }

        System.out.println("cache stats:");
        //最后打印缓存的命中率等 情况
        System.out.println(studentCache.stats().toString());
}
```

#### 4.1 Callable方式

```java
    private static Cache<String, CompanyUser> localCache = CacheBuilder.newBuilder()
            // 并发级别为8
            .concurrencyLevel(8)
            // 写缓存1分钟后过期
            .expireAfterWrite(1, TimeUnit.MINUTES)
            // 缓存初始容量为10
            .initialCapacity(10)
            // 缓存最大容量为100，超过以后按照lru算法移除缓存
            .maximumSize(100)
            // 统计缓存命中率
            .recordStats()
            // 缓存移除时打印日志
            .removalListener(new RemovalListener<Object, Object>() {
                @Override
                public void onRemoval(RemovalNotification<Object, Object> removalNotification) {
                    log.info("guava---key=" + removalNotification.getKey() + " was removed, cause is "
                            + removalNotification.getCause());
                }
            }).build();

    public static void main(String[] args) {
             CompanyUser companyUser = localCache.get(key, new Callable<CompanyUser>() {
                @Override
                public CompanyUser call() {
                    if (redisService.exist(key)) {
                        CompanyUser compUser = redisService.get(key);
                        localCache.put(key, compUser);
                        return compUser;
                    }
                    CompanyUser user = companyUserMapper.findCompanyUserByUserId(userId);
                    if (null != user) {
                        redisService.set(key, user, (int) redisProperties.getDefaultExpiration());
                        localCache.put(key, user);
                    }
                    return user;
                }
            });       
    }
```

参考：

https://www.jianshu.com/p/b3c10fcdbf0f

https://www.cnblogs.com/kabi/p/8310145.html