# redis面试题

## 1.什么是redis？

redis是基于键值对的NoSql内存数据库，redis的值有String、hash、list、set、zset等多种数据结构和算法组成。



## 2.redis的特点

- 速度快，每秒10万次读写操作。
- 基于键值对的数据结构
- 功能丰富，键过期缓存、发布订阅（消息队列）
- 支持对客户端tcp通信协议
- 持久化，RDB和AOF两种
- 主从复制
- 高可用和分布式 
  - 2.8版本，Redis Sentinel，保证节点的故障发现和故障自动迁移；
  - 3.0版本，提供了分布式实现Redis Cluster，真正的分布式，提供了高可用、读写和容量的扩展性。



根据数据量的大小 和数据的冷热来判定是否应该使用redis。



## 3.redis和memcache的区别

- 1、Redis和Memcache都是将数据存放在内存中，都是内存数据库。 redis的string类型值不能超过512MB，而memcached的大小不能超过1MB。
- 2、Redis不仅仅支持简单的k/v类型的数据，同时还提供list，set，hash等数据结构的存储。 
- 3、虚拟内存–Redis当物理内存用完时，可以将一些很久没用到的value 交换到磁盘  
- 4、存储数据安全–memcache挂掉后，数据没了；redis可以定期保存到磁盘（持久化） 
- 5、灾难恢复–memcache挂掉后，数据不可恢复; redis数据丢失后可以通过aof恢复 



## 4.单线程为什么还很快

- 纯内存
- 非阻塞IO
- 单线程避免了线程切换和竞太的消耗