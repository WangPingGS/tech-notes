# Mybatis-plus 简介

MyBatis-Plus（简称 MP）是一个 MyBatis 的增强工具，在 MyBatis 的基础上只做增强不做改变，为简化开发、提高效率而生。

#  特性

- 无侵入：只做增强不做改变，引入它不会对现有工程产生影响，如丝般顺滑
- 损耗小：启动即会自动注入基本 CURD，性能基本无损耗，直接面向对象操作
- 强大的 CRUD 操作：内置通用 Mapper、通用 Service，仅仅通过少量配置即可实现单表大部分 CRUD 操作，更有强大的条件构造器，满足各类使用需求
- 支持 Lambda 形式调用：通过 Lambda 表达式，方便的编写各类查询条件，无需再担心字段写错
- 支持多种数据库：支持 MySQL、MariaDB、Oracle、DB2、H2、HSQL、SQLite、Postgre、SQLServer2005、SQLServer 等多种数据库
- 支持主键自动生成：支持多达 4 种主键策略（内含分布式唯一 ID 生成器 - Sequence），可自由配置，完美解决主键问题
- 支持 XML 热加载：Mapper 对应的 XML 支持热加载，对于简单的 CRUD 操作，甚至可以无 XML 启动
- 支持 ActiveRecord 模式：支持 ActiveRecord 形式调用，实体类只需继承 Model 类即可进行强大的 CRUD操作
- 支持自定义全局通用操作：支持全局通用方法注入（ Write once, use anywhere ）
- 支持关键词自动转义：支持数据库关键词（order、key......）自动转义，还可自定义关键词
- 内置代码生成器：采用代码或者 Maven 插件可快速生成 Mapper 、 Model 、 Service 、 Controller 层代码，支持模板引擎，更有超多自定义配置等您来使用
- 内置分页插件：基于 MyBatis 物理分页，开发者无需关心具体操作，配置好插件之后，写分页等同于普通List 查询
- 内置性能分析插件：可输出 Sql 语句以及其执行时间，建议开发测试时启用该功能，能快速揪出慢查询
- 内置全局拦截插件：提供全表 delete 、 update 操作智能分析阻断，也可自定义拦截规则，预防误操作
- 内置 Sql 注入剥离器：支持 Sql 注入剥离，有效预防 Sql 注入攻击

# 配置

虽然在MybatisPlus中可以实现零配置，但是有些时候需要我们自定义一些配置，就需要使用Mybatis原生的一些配置文件方式了。

```properties
# 指定全局配置文件
mybatis-plus.config-location = classpath:mybatis-config.xml
# 指定mapper.xml文件
mybatis-plus.mapper-locations = classpath*:mybatis/*.xml
```

<https://mp.baomidou.com/guide/config.html>



# 示例

```java
package com.wp.mybaitsplus.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author wangping
 * @since 2019/4/11 18:03
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class User {
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    private String name;
    private Integer age;
    private String email;
}
```

```java
@Test
public void testSelect() {
    List<User> users = userMapper.selectList(null);
    for (User user : users) {
        System.out.println(user);
    }
    Assert.assertEquals(5, users.size());
}

/**
 * 插入一条数据
 */
@Test
public void insertTest() {
    User user = new User();
    user.setAge(22);
    user.setEmail("test8@wp.com");
    user.setName("wp8");
    int result = userMapper.insert(user);
    System.out.println(result);
}

/**
 * 修改数据
 */
@Test
public void updateTest() {
    User user = User.builder().id(2L).name("zhangsan").build();
    int result = userMapper.updateById(user);
    System.out.println(result);
}
/**
 * like 查询
 */
@Test
public void likeTest() {
    QueryWrapper<User> wrapper = new QueryWrapper<>();
    // 查询名字中包含o的用户
    wrapper.like("name", "o");
    List<User> users = userMapper.selectList(wrapper);
    for (User user : users) {
        System.out.println(user);
    }
}
/**
 * 条件查询
 */
@Test
public void test2() {
    // 查询年龄小于等于20的用户
    QueryWrapper<User> wrapper = new QueryWrapper<>();
    wrapper.le("age", 20);
    List<User> users = userMapper.selectList(wrapper);
    for (User user : users) {
        System.out.println(user);
    }
}
/**
 * 分页查询
 */
@Test
public void test3() {
    Page<User> page = new Page<>(1, 2);
    IPage<User> userIPage = userMapper.selectPage(page, null);
    System.out.println("总条数：" + userIPage.getTotal());
    System.out.println("当前页数：" + userIPage.getCurrent());
    System.out.println("每页显示记录数：" + userIPage.getSize());
    List<User> users = userIPage.getRecords();
    for (User user : users) {
        System.out.println(user);
    }
}
```



# 依赖

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter</artifactId>
</dependency>

<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-test</artifactId>
    <scope>test</scope>
</dependency>

<!--简化代码的工具包-->
<dependency>
    <groupId>org.projectlombok</groupId>
    <artifactId>lombok</artifactId>
    <optional>true</optional>
</dependency>
<!--mybatis-plus的springboot支持-->
<dependency>
    <groupId>com.baomidou</groupId>
    <artifactId>mybatis-plus-boot-starter</artifactId>
    <version>3.0.5</version>
</dependency>
<!--mysql驱动-->
<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
    <version>5.1.47</version>
</dependency>
```



代码：<https://gitlab.com/WangPingGS/tech/tree/master/mybaits-plus>