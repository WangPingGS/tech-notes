# 1 介绍

MQ全称为Message Queue，即消息队列， RabbitMQ是由erlang语言开发，基于AMQP（Advanced Message
Queue 高级消息队列协议）协议实现的消息队列，它是一种应用程序之间的通信方法，消息队列在分布式系统开
发中应用非常广泛。RabbitMQ官方地址：http://www.rabbitmq.com/



开发中消息队列通常有如下应用场景：
1、任务异步处理。
将不需要同步处理的并且耗时长的操作由消息队列通知消息接收方进行异步处理。提高了应用程序的响应时间。
2、应用程序解耦合
MQ相当于一个中介，生产方通过MQ与消费方交互，它将应用程序进行解耦合。



市场上还有哪些消息队列？
ActiveMQ，RabbitMQ，ZeroMQ，Kafka，MetaMQ，RocketMQ、Redis。

为什么使用RabbitMQ呢？
1、使得简单，功能强大。
2、基于AMQP协议。
3、社区活跃，文档完善。
4、高并发性能好，这主要得益于Erlang语言。
5、Spring Boot默认已集成RabbitMQ



# 2 快速入门



## 2.1 快速理解

下图是Rabbitmq的基本结构：

![1557880578491](./images\1557880578491.png)



组成部分说明如下：

- Broker：消息队列服务进程，此进程包括两个部分：Exchange和Queue。
- Exchange：消息队列交换机，按一定的规则将消息路由转发到某个队列，对消息进行过虑。
- Queue：消息队列，存储消息的队列，消息到达队列并转发给指定的消费方。
- Producer：消息生产者，即生产方客户端，生产方客户端将消息发送到MQ。
- Consumer：消息消费者，即消费方客户端，接收MQ转发的消息。



消息发布接收流程：
-----发送消息-----

1. 生产者和Broker建立TCP连接。
2. 生产者和Broker建立通道。
3. 生产者通过通道消息发送给Broker，由Exchange将消息进行转发。
4. Exchange将消息转发到指定的Queue（队列）



----接收消息-----

1. 消费者和Broker建立TCP连接。
2. 消费者和Broker建立通道。
3. 消费者监听指定的Queue（队列）。
4. 当有消息到达Queue时Broker默认将消息推送给消费者。
5. 消费者接收到消息。



##  2.2 windows安装



### 2.2.1 下载安装



RabbitMQ由Erlang语言开发，Erlang语言用于并发及分布式系统的开发，在电信领域应用广泛，OTP（Open
Telecom Platform）作为Erlang语言的一部分，包含了很多基于Erlang开发的中间件及工具库，安装RabbitMQ需
要安装Erlang/OTP，并保持版本匹配，如下图：
RabbitMQ的下载地址：http://www.rabbitmq.com/download.html



![1557881131128](./images\1557881131128.png)



本次使用Erlang/OTP 20.3版本和RabbitMQ3.7.3版本。



1）下载erlang
地址如下：
http://erlang.org/download/otp_win64_20.3.exe
以管理员方式运行此文件，安装。
erlang安装完成需要配置erlang环境变量： ERLANG_HOME=D:\Program Files\erl9.3 在path中添
加%ERLANG_HOME%\bin;

2）安装RabbitMQ
https://github.com/rabbitmq/rabbitmq-server/releases/tag/v3.7.3
以管理员方式运行此文件，安装。

或软件在网盘中：

https://pan.baidu.com/s/16CLMGIzU4hqmGRrWRXi0Zg



### 2.2.2 启动



安装成功后会自动创建RabbitMQ服务并且启动。

1）从开始菜单启动RabbitMQ

完成在开始菜单找到RabbitMQ的菜单：

RabbitMQ Service-install :安装服务

RabbitMQ Service-remove 删除服务

RabbitMQ Service-start 启动

RabbitMQ Service-stop 启动



2）如果没有开始菜单则进入安装目录下sbin目录手动启动

rabbitmq-service.bat install 安装服务

rabbitmq-service.bat stop 停止服务

rabbitmq-service.bat start 启动服务



### 2.2.3 安装后台管理插件



安装rabbitMQ的管理插件，方便在浏览器端管理RabbitMQ

管理员身份在命令窗口运行 rabbitmq-plugins.bat enable rabbitmq_management

然后重启服务

进入浏览器，输入：http://localhost:15672，用户名/密码：guest/guest



### 2.2.4 注意事项



1、安装erlang和rabbitMQ以管理员身份运行。

2、当卸载重新安装时会出现RabbitMQ服务注册失败，此时需要进入注册表清理erlang

搜索RabbitMQ、ErlSrv，将对应的项全部删除。



## 2.3 使用 Rabbitmq java client 实现demo



我们先用 rabbitMQ官方提供的java client测试，目的是对RabbitMQ的交互过程有个清晰的认识。

创建生产者工程和消费者工程，分别加入RabbitMQ java client的依赖。

代码如下：

<https://gitlab.com/WangPingGS/tech/tree/master/rabbitmq-demo>

项目：rabbitmq-producer和rabbitmq-consumer便是。



# 3 工作模式



RabbitMQ有以下几种工作模式 ：

1. Work queues
2. Publish/Subscribe
3. Routing
4. Topics
5. Header
6. RPC



## 3.1 Work queues



![1557882748746](./images\1557882748746.png)

work queues与入门程序相比，多了一个消费端，两个消费端共同消费同一个队列中的消息。

应用场景：对于 任务过重或任务较多情况使用工作队列可以提高任务处理的速度。

测试：

1、使用入门程序，启动多个消费者。

2、生产者发送多个消息。

结果：

1、一条消息只会被一个消费者接收；

2、rabbit采用轮询的方式将消息是平均发送给消费者的；

3、消费者在处理完某条消息后，才会收到下一条消息。



代码如下：

<https://gitlab.com/WangPingGS/tech/tree/master/rabbitmq-demo>

项目：rabbitmq-producer和rabbitmq-consumer下work包便是。



## 3.2 Publish/subscribe



![1557882933665](./images\1557882933665.png)

发布订阅模式：

1、每个消费者监听自己的队列。

2、生产者将消息发给broker，由交换机将消息转发到绑定此交换机的每个队列，每个绑定交换机的队列都将接收

到消息

案例：

用户通知，当用户充值成功或转账完成系统通知用户，通知方式有短信、邮件多种方法 。

代码如下：

<https://gitlab.com/WangPingGS/tech/tree/master/rabbitmq-demo>

项目：rabbitmq-producer和rabbitmq-consumer下pub包便是。



生产者：

声明Exchange_fanout_inform交换机。交换机类型BuiltinExchangeType.FANOUT

声明两个队列并且绑定到此交换机，绑定时不需要指定routingkey

发送消息时不需要指定routingkey



消费者：

声明Exchange_fanout_inform交换机。交换机类型BuiltinExchangeType.FANOUT

声明一个队列并且绑定到此交换机，绑定时不需要指定routingkey



### 3.2.1 结论



1、publish/subscribe与work queues有什么区别。

区别：

1）work queues不用定义交换机，而publish/subscribe需要定义交换机。

2）publish/subscribe的生产方是面向交换机发送消息，work queues的生产方是面向队列发送消息(底层使用默认交换机)。

3）publish/subscribe需要设置队列和交换机的绑定，work queues不需要设置，实质上work queues会将队列绑

定到默认的交换机 。

相同点：

所以两者实现的发布/订阅的效果是一样的，多个消费端监听同一个队列不会重复消费消息。

2、实质工作用什么 publish/subscribe还是work queues。

建议使用 publish/subscribe，发布订阅模式比工作队列模式更强大，并且发布订阅模式可以指定自己专用的交换

机。



## 3.3  Routing



![1557883607485](./images\1557883607485.png)

路由模式：

1、每个消费者监听自己的队列，并且设置routingkey。

2、生产者将消息发给交换机，由交换机根据routingkey来转发消息到指定的队列。



生产者：

声明exchange_routing_inform交换机，交换机类型：BuiltinExchangeType.DIRECT

声明两个队列并且绑定到此交换机，绑定时需要指定routingkey

发送消息时需要指定routingkey



消费者：

声明exchange_routing_inform交换机，交换机类型：BuiltinExchangeType.DIRECT

声明一个队列并且绑定到此交换机，绑定时需要指定routingkey



代码如下：

<https://gitlab.com/WangPingGS/tech/tree/master/rabbitmq-demo>

项目：rabbitmq-producer和rabbitmq-consumer下routing包便是。



### 3.3.1 结论

Routing模式和Publish/subscibe有啥区别？

Routing模式要求队列在绑定交换机时要指定routingkey，消息会转发到符合routingkey的队列。而Publish/subscibe指定routingkey为空。



## 3.4 Topics通配符



![1557884141465](./images\1557884141465.png)

Topics通配符模式：

1. 每个消费者监听自己的队列，并且设置带统配符的routingkey。
2. 生产者将消息发给broker，由交换机根据routingkey来转发消息到指定的队列。



案例：

根据用户的通知设置去通知用户，设置接收Email的用户只接收Email，设置接收sms的用户只接收sms，设置两种

通知类型都接收的则两种通知都有效。



生产者：

声明交换机，类型为BuiltinExchangeType.TOPIC

声明两个队列并且，不需要绑定到交换机

发送消息时需要指定具体routingkey，如：

- 只发送到短信队列，routingkey为infoma.sms
- 只发送到邮箱队列，routingkey为infoma.email
- 都发，routingkey为infoma.email.sms或infoma.sms.email



```
infoma.#.sms.#  eg:infoma.email.sms,infoma.sms

infoma.*.sms  eg:必须满足infoma.xx.sms, 而infoma.sms不符
```



消费端通配符如下：

```java
private static final String ROUTING_KEY_SMS = "infoma.#.sms.#";
private static final String ROUTING_KEY_EMAIL = "infoma.#.email.#";
```

消费者：在绑定交换机和队列时指定路由key。



代码如下：

<https://gitlab.com/WangPingGS/tech/tree/master/rabbitmq-demo>

项目：rabbitmq-producer和rabbitmq-consumer下topic包便是。



### 3.4.1 结论

本案例的需求使用Routing工作模式能否实现？

使用Routing模式也可以实现本案例，共设置三个 routingkey，分别是email、sms、all，email队列绑定email和

all，sms队列绑定sms和all，这样就可以实现上边案例的功能，实现过程比topics复杂。

Topic模式更多加强大，它可以实现Routing、publish/subscirbe模式的功能。



## 3.5 Header模式



header模式与routing不同的地方在于，header模式取消routingkey，使用header中的 key/value（键值对）匹配

队列。

案例：

根据用户的通知设置去通知用户，设置接收Email的用户只接收Email，设置接收sms的用户只接收sms，设置两种

通知类型都接收的则两种通知都有效。



代码：

1.生产者

队列与交换机绑定方式  和之前不同

```java
Map<String, Object> headers_email = new Hashtable<String, Object>();
headers_email.put("inform_type", "email");
Map<String, Object> headers_sms = new Hashtable<String, Object>();
headers_sms.put("inform_type", "sms");
channel.queueBind(QUEUE_INFORM_EMAIL,EXCHANGE_HEADERS_INFORM,"",headers_email);
channel.queueBind(QUEUE_INFORM_SMS,EXCHANGE_HEADERS_INFORM,"",headers_sms);
```



通知：

```java
String message = "email inform to user"+i;
Map<String,Object> headers = new Hashtable<String, Object>();
headers.put("inform_type", "email");//匹配email通知消费者绑定的header
//headers.put("inform_type", "sms");//匹配sms通知消费者绑定的header
AMQP.BasicProperties.Builder properties = new AMQP.BasicProperties.Builder();
properties.headers(headers);
//Email通知
channel.basicPublish(EXCHANGE_HEADERS_INFORM, "", properties.build(), message.getBytes());
```



2.邮件消费者

```java
channel.exchangeDeclare(EXCHANGE_HEADERS_INFORM, BuiltinExchangeType.HEADERS);
Map<String, Object> headers_email = new Hashtable<String, Object>();
headers_email.put("inform_email", "email");
//交换机和队列绑定
channel.queueBind(QUEUE_INFORM_EMAIL,EXCHANGE_HEADERS_INFORM,"",headers_email);
//指定消费队列
channel.basicConsume(QUEUE_INFORM_EMAIL, true, consumer);
```



测试：

![1557887361429](./images\1557887361429.png)



## 3.6 RPC



![1557888515575](./images\1557888515575.png)



RPC即客户端远程调用服务端的方法 ，使用MQ可以实现RPC的异步调用，基于Direct交换机实现，流程如下：

1、客户端即是生产者就是消费者，向RPC请求队列发送RPC调用消息，同时监听RPC响应队列。

2、服务端监听RPC请求队列的消息，收到消息后执行服务端的方法，得到方法返回的结果

3、服务端将RPC方法 的结果发送到RPC响应队列

4、客户端（RPC调用方）监听RPC响应队列，接收到RPC调用结果。



# 4. Springboot整合Rabbitmq



 	