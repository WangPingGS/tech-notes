## docker镜像

> 1.获取镜像

```java
    docker pull imageName:tag
    // imageName：镜像名称，tag：镜像标签
    // 如果不指定镜像标签tag，默认获取latest标签的镜像
```

> 2.查看镜像

```java
    docker images
```

> 3.使用tag命令添加镜像标签，镜像名称改变，镜像id相同，仍然使用同一个镜像文件

```java
    docker tag centos:latest mycentos:latest
```

> 4.使用inspect命令查看详细信息

```java
    docker inspect centos
    // 返回json数据，通过-f 参数来获取json中的其中一项信息。
```

> 5.使用history命令查看镜像历史，列出镜像各层的创建信息

```java
    docker history centos:latest
    // 使用--no-trunc参数输出完整信息
```

> 6.搜索镜像

```java
    docker search centos
    // 搜索带关键字centos的镜像
```

> 7.删除镜像

```java
    // a.使用标签tag删除，当有多个标签时，只删除当前标签，而实际的镜像并未删除；当镜像只有一个标签时，会删除相应的镜像。
    docker rmi centos:latest

    // b.使用镜像Id删除，会删除所有的标签，并删除镜像本身

    // c.当镜像的容器存在时，镜像本身无法删除。如果想强制删除，使用参数-f

    // 正确做法是先删除容器（docker rm dockerId），再删除镜像
```

> 8.创建镜像

```java
    // a.基于已有镜像的容器创建
    docker commit -m "commit message" -a "author" dockerId imageName:tag
    // eg:docker commit -m "add a new file" -a "wp" ca55a31eb5a5 test:0.1

    // b.基于本地模板导入,模板下载地址：https://openvz.org/Download/template/precreated
    cat slackware-12.0-i386-minimal.tar.gz | docker import -slackware:0.0.1

    // c.使用Dockerfile创建镜像
    docker build -t dockerfile .
    // 最后的点.表示当前目录。可以指定dockerfile文件的目录。
```

> 9.存出和载入镜像

```java
    // a.存出，导出成压缩包
    docker save -o test_0.1.tar test:0.1

    // b.载入，压缩包导入成镜像
    docker load --input test_0.1.tar
```

> 10.上传镜像

```java
    docker push imageName:tag | repository host:port/imageName:tag
```