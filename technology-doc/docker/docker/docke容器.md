## docke容器

> 1.新建容器

```java
    docker create -it centos:latest
```

> 2.启动容器

```java
    docker start dockerId
```

> 3.新建并启动容器

```java
    // 创建并启动容器centos，运行程序后容器停止。
    docker run centos /bin/echo 'Hello world'

    // 创建并启动容器，开启一个bash终端，允许用户进行交互，exit退出终端，容器停止。
    docker run -it centos /bin/bash

    // 后台启动一个容器，容器一直运行。
    docker run -d -it centos

    // 重启容器
    docker restart dockerId | dockerName
```

> 4.停止容器

```java
    docker stop dockerId | dockerName
    // docker kill命令会强制终止容器。
```

> 5.进入容器

```java
    // a.attach命令
    docker attach dockerId | dockerName
    // attach命令会进入容器启动命令的终端，退出终端意味着容器停止，可以查看容器启动的输出信息。不能启动新的进程。
    docker logs -f dockerId
    // 也可以通过此命令查看容器启动的输出信息。

    // b.exec命令
    docker exec -it dockerId | dockerName bash
    // 在容器中打开新的终端，并且可以启动新的进程。退出不会停止容器。
```

> 6.删除容器

```java
    docker rm dockerId | dockerName
    // 删除容器，只能删除终止和退出状态的容器。rm后跟-f参数即可强制删除。

    docker ps -a
    // 可以查看终止的容器，可以将终止的容器删除。
```

> 7.导入导出容器

```java
    // a.导出容器
    docker export -o test.tar dockerId
    // 或
    docker export dockerId > test.tar

    // b.导入容器
    docker import test.tar -test/centos:0.1
    // 跟镜像载入docker load很相似，一个内容是镜像文件到本地镜像库，一个是容器快照到本地镜像库。容器快照文件将丢失所有的历史记录和元数据信息，而镜像文件保存完整记录。容器快照导入时可重新指定标签等元数据信息。
```


