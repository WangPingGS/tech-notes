## 1、了解IOC吗？

IOC控制翻转

- Spring Core最核心的部分
- 需要先了解依赖注入
  - 基于接口编程
  - 上层建筑不依赖于下层建筑，而是依赖于抽象
  - 含义：把底层类作为参数传递给上层类，实现上层对下层的控制

![1552029426713](./images\1552029426713.png)





![1552029502072](./images\1552029502072.png)

### 1.1 依赖倒置原则、IOC、DI、IOC容器的关系

![1552029646965](./images\1552029646965.png)

- 依赖倒置原则：高层模块不应该依赖于底层模块，两者应该依赖于抽象。
- IOC容器：管理bean的生命周期，控制bean的依赖注入



### 1.2 IOC容器的优势

- 避免各处使用new来创建，并且可以做到统一维护
- 创建的时候隐藏了依赖建立的细节，交给工厂完成

![1552030185736](./images\1552030185736.png)

![1552030257872](./images\1552030257872.png)

Spring IOC的核心接口：

- BeanFactory
- ApplicationContext

BeanDefinition：

- 描述bean的定义

BeanDefinitionRegistry:

- 提供向容器注册BeanDefinition的方法registryBeanDefinition(); 存放到DefaultListableBeanFactory的beanDefinitionMap中；beanName为key；map为ConcurrentHashMap中；同时将beanName存入到beanDefinitionNames列表中（arraylist类型），以便后期bean的实例化。

BeanFactory：

- 提供IOC的配置机制
- 包含了各种bean的定义，便于实例化bean
- 建立bean的依赖关系 
- bean生命周期的控制

![1552030849273](./images\1552030849273.png)

ListableBeanFactory:定义了访问容器中bean基本信息的若干方法，如查看bean的个数，获取某一类bean的配置名，查看容器中是否包含某一bean；

HierarchicalBeanFactory： 父子级联的Ioc容器接口，此容器可以通过接口方法访问父容器；通过该类，SpringIOC容器可以建立父子层级的容器体系，子容器可以访问父容器中的bean，父容器不能访问子容器中的bean。比如在SpringMVC中，展现层的bean位于一个子容器中，而业务层和持久层的bean位于父容器中，这样展现层的bean就可以引入业务层和持久层的bean，而业务层和持久层的bean看不到展现层的bean。

ConfigurableBeanFactory：增强了IOC容器的可定制性，定义了设置类装载器，属性编辑器以及属性初始化、后置处理器等方法。

AutowireCapableBeanFactory：定义了容器中的bean按某种规则（名字匹配、类型匹配等）对bean进行自动装配；

SingletonBeanRegistry：允许在运行期间向容器注册singleton实例bean的方法；

BeanFactory：

- getBean
- isSingleton
- isPrototype，如果为true，那么getBean时候，会新建一个bean。



Beanfactory和ApplicationContext的比较

- BeanFactory是spring框架的基础设施，面向Spring本身
- ApplicationContext面向使用Spring框架的开发者



ApplicationContext的功能（继承多个接口）

- BeanFactory：能够管理、装配bean
- ResourcePatternResolver：能够加载资源文件
- MessageSource：能够实现国际化等功能
- ApplicationEventPublisher：能够注册监听器，实现监听机制（可以发布事件给注册的listener）

UML关系图如下：

![1552032506171](./images\1552032506171.png)



SpringBoot启动类中run方法逐层进入，最后找到会选择AnnotationConfigServletApplicationContext。



实战：

Bean是如何装载到IOC的？如何从IOC获取bean实例并使用？深入分析IOC容器的创建配置以及bean如何获取的底层原理。



getBean方法的代码逻辑

- doGetBean，根据name转换成beanName
- 通过beanName调getSingleton(beanName)获取共享实例；bean的实例或者是工厂的实例
- getObjectForBeanInstance方法，尝试从缓存或实例工厂中去获取实例；
- 获取不到时，从parentBeanFactory工厂获取，子类和父类的定义进行合并RootBeanDefinition，将父类的属性合并到子类里面；
- dependsOn ！= null ，存在依赖，那么需要递归实例化依赖的bean
- bean的作用域是否是singleton，从缓存中获取，缓存中没有则创建
- bean的作用域是prototype，直接new 一个bean；如果其他方式创建；
- 判断需要的类是否符合实际bean的类型，然后返回bean。



![1552034437868](./images\1552034437868.png)



面试题：

1.springbean的作用域

![1552035297541](./images\1552035297541.png)



2.springbean的生命周期

bean的生命周期是由Spring容器管理的，创建和销毁。 



![1552035427004](./images\1552035427004.png)

- 实例化bean
- 通过各种Aware接口声明依赖关系，则会注入bean基础设施成员的依赖，是为了能够感知到自身属性。容器管理的bean一般不需要了解容器的状态和直接使用容器，但是在某些情况下直接在bean中对ioc容器进行操作的， 这时候需要在bean中设置对容器的感知，SpringIoc容器也提供了该功能，是通过特定的Aware接口完成的。BeanNameAware：可以在bean中得到他在ioc容器中bean的实例名字 。
- BeanPostProcess.postProcessBeforeInitialization()前置初始化方法，Spring完成实例化之后对bean添加自定义的逻辑。
-  InitialzingBean.afterPropertiesSet，如果实现了InitialzingBean接口，则会调用afterPropertiesSet方法，做些属性设置之后的自定义事情。
- 调用Bean的Init方法，去完成初始化相关的工作；
- BeanPostProcess.postProcessAfterInitialization后置初始化方法，实例初始化之后的自定义工作，
- Bean创建完毕。



销毁过程：

- 若实现了DisposableBean接口，则会调用destory方法
- 若配置了destry-method属性，则会调用其配置的销毁方法



## 2、了解AOP吗？

![1552045495534](./images\1552045495534.png)

![1552045567008](./images\1552045567008.png)

AOP使用：（what + where + when）

![1552046204590](./images\1552046204590.png)

![1552046267515](./images\1552046267515.png)



AOP的原理：jdkProxy和Cglib

- proxy：通过反射接收被代理的类，并且要求被代理的类必须实现一个接口
- Cglib通过修改字节码的方式生成代理对象的。继承的方式，如果某个类是final修饰，那么不能使用cglib方式

![1552046564208](./images\1552046564208.png)

![1552046598213](./images\1552046598213.png)



![1552047190303](./images\1552047190303.png)



Spring源码分析：

AbstractBeanFactory中的doGetBean()

AbstractAutowireCapableBeanFactory中的doCreateBean

initializeBean，会调用beanPostProcessor中的方法（实现类是AbstractAutoProxyCreator）