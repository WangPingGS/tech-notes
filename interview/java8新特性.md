## java8新特性

Java8新增了很多新特性，如

- **Lambda 表达式** − Lambda允许把函数作为一个方法的参数（函数作为参数传递进方法中）。使用 Lambda 表达式可以使代码变的更加简洁紧凑。
- **方法引用** − 方法引用提供了非常有用的语法，可以直接引用已有Java类或对象（实例）的方法或构造器。与lambda联合使用，方法引用可以使语言的构造更紧凑简洁，减少冗余代码。
- **默认方法** − 默认方法就是一个在接口里面有了一个实现的方法。
- **Stream API** −新添加的Stream API（java.util.stream） 把真正的函数式编程风格引入到Java中。
- **Date Time API** − 加强对日期与时间的处理。
- **Optional 类** − Optional 类已经成为 Java 8 类库的一部分，用来解决空指针异常。



### 1. lambda 表达式

Lambda 允许把函数作为一个方法的参数（函数作为参数传递进方法中）。

使用 Lambda 表达式可以使代码变的更加简洁紧凑。

https://www.runoob.com/java/java8-lambda-expressions.html

1. 语法

```java
(parameters) -> expression
或
(parameters) ->{ statements; }
```

2. 特征


> * 可选类型声明：不需要声明参数类型，编译器可以统一识别参数值。
> * 可选的参数圆括号：一个参数无需定义圆括号，但多个参数需要定义圆括号。
> * 可选的大括号：如果主体包含了一个语句，就不需要使用大括号。
> * 可选的返回关键字：如果主体只有一个表达式返回值则编译器会自动返回值，大括号需要指定明表达式返回了一个数值。

```java
    // 1. 不需要参数,返回值为 5  
    () -> 5  
    
    // 2. 接收一个参数(数字类型),返回其2倍的值  
    x -> 2 * x  
    
    // 3. 接受2个参数(数字),并返回他们的差值  
    (x, y) -> x – y  
    
    // 4. 接收2个int型整数,返回他们的和  
    (int x, int y) -> x + y  
    
    // 5. 接受一个 string 对象,并在控制台打印,不返回任何值(看起来像是返回void)  
    (String s) -> System.out.print(s)
```

3. 代码示例

```java
public class Java8Tester {
   public static void main(String args[]){
      Java8Tester tester = new Java8Tester();
        
      // 类型声明
      MathOperation addition = (int a, int b) -> a + b;
        
      // 不用类型声明
      MathOperation subtraction = (a, b) -> a - b;
        
      // 大括号中的返回语句
      MathOperation multiplication = (int a, int b) -> { return a * b; };
        
      // 没有大括号及返回语句
      MathOperation division = (int a, int b) -> a / b;
        
      System.out.println("10 + 5 = " + tester.operate(10, 5, addition));
      System.out.println("10 - 5 = " + tester.operate(10, 5, subtraction));
      System.out.println("10 x 5 = " + tester.operate(10, 5, multiplication));
      System.out.println("10 / 5 = " + tester.operate(10, 5, division));
        
      // 不用括号
      GreetingService greetService1 = message ->
      System.out.println("Hello " + message);
        
      // 用括号
      GreetingService greetService2 = (message) ->
      System.out.println("Hello " + message);
        
      greetService1.sayMessage("Runoob");
      greetService2.sayMessage("Google");
   }
    
   interface MathOperation {
      int operation(int a, int b);
   }
    
   interface GreetingService {
      void sayMessage(String message);
   }
    
   private int operate(int a, int b, MathOperation mathOperation){
      return mathOperation.operation(a, b);
   }
}
```

4. 变量作用域

> * lambda 表达式只能引用标记了 final 的外层局部变量，这就是说不能在 lambda 内部修改定义在域外的局部变量，否则会编译错误。
> * 可以直接在 lambda 表达式中访问外层的局部变量
> * lambda 表达式的局部变量可以不用声明为 final，但是必须不可被后面的代码修改（即隐性的具有 final 的语义）
> 

### Stream API

将要处理的元素集合转换成一种流， 流在管道中传输， 并且可以在管道的节点上进行处理， 比如筛选， 排序，聚合等。

- 元素是特定类型的对象，形成一个队列。 Java中的Stream并不会存储元素，而是按需计算。
- **数据源** 流的来源。 可以是集合，数组，I/O channel， 产生器generator 等。
- **聚合操作** 类似SQL语句一样的操作， 比如filter, map, reduce, find, match, sorted等。
  - forEach，Stream 提供了新的方法 'forEach' 来迭代流中的每个数据；
  - map，用于将集合中的元素按某种方式转换成另一个元素；
  - filter，通过设置条件来过滤元素，可以结合Predicate一起使用；
  - limit，获取指定数量的流；
  - sorted，对流中的元素排序。

在 Java 8 中, 集合接口有两个方法来生成流：

- **stream()** − 为集合创建串行流。
- **parallelStream()** − 为集合创建并行流。



Collections

Collectors 类实现了很多归约操作，例如将流转换成集合和聚合元素.

```java
List<String>strings = Arrays.asList("abc", "", "bc", "efg", "abcd","", "jkl");
List<String> filtered = strings.stream().filter(string -> !string.isEmpty()).collect(Collectors.toList());
 
System.out.println("筛选列表: " + filtered);
String mergedString = strings.stream().filter(string -> !string.isEmpty()).collect(Collectors.joining(", "));
System.out.println("合并字符串: " + mergedString);
```





```java
        // 定义filter
        Predicate<CompanyUserRepo> addFilter = (v) -> (SyncTag.ADD.equals(v.getTag()));
        Predicate<CompanyUserRepo> updateFilter = (v) -> (SyncTag.UPDATE.equals(v.getTag()));
        Predicate<CompanyUserRepo> deleteFilter = (v) -> (SyncTag.DELETE.equals(v.getTag()));

        List<CompanyUserRepo> repos = currentUsers.values().stream().filter(addFilter.or(updateFilter).or(deleteFilter))
                .collect(Collectors.toList());
        List<CompanyUser> addList = repos.stream().filter(addFilter).map(t -> (from(t)))
                .collect(Collectors.toList());
        List<CompanyUser> updateList = repos.stream().filter(updateFilter).map(t -> from(t))
                .collect(Collectors.toList());
        List<CompanyUser> deleteList = repos.stream().filter(deleteFilter).map(t -> from(t))
                .collect(Collectors.toList());
```

