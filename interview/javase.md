# 1、Java基础知识

1. 方法重载

> * 在同一个类中；
> * 方法名相同；
> * 参数不同（类型、个数、顺序）；
> * 与访问修饰符合返回值类型无关。

2. 类的组成部分

> * 静态的特征（成员变量（静态变量和实例变量））；
> * 动态的行为（静态方法和实例方法）；
> * 构造方法、代码块（静态代码块和代码块）、内部类。

3. 局部变量和成员变量区别

> * 位置不同；
> * 作用范围不同；
> * 初始值不同；
> * 内存存储位置不同。

4. 特殊属性内存存储位置

> * static 修饰的成员变量，存在方法区中；

5. 方法重写

> * 方法名相同，参数必须相同，返回值相同；
> * 重写方法不能使用比被重写方法更严格的访问权限。（由于多态）；
> * 抛出异常不能是父类异常。如父类方法抛出异常是RuntimeException，子类重写的方法抛出异常不能是Exception。

6. 创建对象

> * 创建一个子类对象，会自上而下分别创建Object，父类对象和该对象；
> * 每个对象除了具有自身的成员变量外，都还有分配this和super两个属性空间；（super指向当前对象的直接父类对象，父类对象和子类对象依靠super关键字联系起来）

7. 继承条件下对象的创建过程

> * 第一语句默认是super(),代表父类的无参数构造方法;
> * 也可以显式指定父类的其他构造方法 super(color,age);
> * 也可以显式的指定当前对象的其他构造方法 this();

> this和super不能同时出现在子类的构造方法中,this和super都必须是构造方法的第一条语句

8. 封装涉及的权限修饰符有哪些

> * private  当前类              类可见性
> * 默认  当前包          包可见性
> * protected  当前包 + 其他包中的子类
> * public   当前包+其他包=所有包=当前项目    项目可见性

9. 异常

* RuntimeException

> * ClassCastException: 父类对象（父类引用指向的是子类对象）转换子类时，类型不匹配，抛出该异常；instanceof 提前判断；
> * ArrayIndexOutOfBoundsException 数组下标越界
> * NullPointerException


* CheckedException

> * FileNotFoundException
> * IOException
> * SQLException
> * ParseException （日期）转换异常
> * UnknownHostException 未知host异常


1.  抽象类和接口的联系和区别

* 共同点

> * 都不能new
> * 都可以有抽象方法
> * 都用来被继承或实现

* 区别

> * 接口只能有全局静态常量，没有变量
> * 接口中全部是抽象方法，抽象类中有0或多个抽象方法
> * 接口中没有构造方法，抽象类中有构造方法
> * 一个类只能继承一个抽象类，但是可以同时实现多个接口

11. 内部类

> * 内部类方法可以直接访问外部类的成员变量和方法；
> * 外部类方法中不能直接访问内部类的属性和方法，可以间接访问（先创建内部类对象）；
> * 内部类的作用（可以定义比private权限更小的作用范围，通过内部类可以实现多重继承）
> * 内部类的编译结果（OuterClass.class OuterClass$InnerClass.class）
> * 成员内部类*、匿名内部类*、静态内部类、方法内部类（如果某些类只使用一次，就没有必须定义一个专门的类，此时使用匿名内部类即可，比如动态代理中没有必要写一个实现接口的类，只需匿名内部类创建接口对应的实例即可）
> * 匿名内部类如何访问外部类的变量（需要将外部类的变量使用final修饰，jdk1.8之后final可以不写）

```java
    // Product是接口：
    Product p2 = new Product(){
        @Override
        public int getPrice() {				
            return 5000;
        }
        @Override
        public String getName() {
            return "apple6plus";
        }			
	};
	System.out.println(p2.getName());

```

```java
    // 外部类，内部类，内部类方法中有同名变量，如何表示
	System.out.println("num="+num);//30
	System.out.println("num="+this.num);//40 20
	System.out.println("num="+OuterClass.this.num);//10

    // 如何创建内部类对象
    OuterClass oc2 = new OuterClass();
	OuterClass.InnerClass ic = oc2.new InnerClass();
	OuterClass.InnerClass ic = new OuterClass().new InnerClass();

```

# 2、Java的反射机制

## 2.1 java的反射机制是什么？

首先大家应该先了解两个概念，**编译期**和**运行期**，编译期就是编译器帮你把源代码翻译成机器能识别的代码，比如编译器把java代码编译成jvm识别的字节码文件，而运行期指的是将可执行文件交给操作系统去执行，JAVA反射机制是在运行状态中，对于任意一个类，都能够知道这个类的所有属性和方法；对于任意一个对象，都能够调用它的任意方法和属性；**这种动态获取信息以及动态调用对象方法的功能称为java语言的反射机制**。

- 编译期，将源代码编译成jvm可识别的字节码文件；
- 运行器，将字节码文件交给jvm去执行。



http://blog.51cto.com/4247649/2109128



 





