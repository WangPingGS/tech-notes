## concept 概念

1. 锁（synchronized）

> * 对象锁，同步代码块或同步方法；多个线程使用的对象不一样，意味着不是同一个锁，无法实现同步。
> * 类锁，同步静态方法。

2. volatile

> * 使得变量在多个线程间可见；
> * 强制线程到主内存去读取变量，而不是去线程工作内存区去读，从而实现了多个线程间变量可见，也满足了线程安全的可见性；
> * 当多个线程使用同一变量时，为了线程安全，通常我们会在访问变量的地方加一把锁，但是这样效率比较低。Volatile的效率相对高点。

```java
package com.wp.test;
public class RunThread extends Thread{
    /**volatile*/
    private volatile boolean isRunning = true;
    public void setRunning(boolean isRunning){
        this.isRunning = isRunning;
    }
    public void run(){
        System.out.println("进入run方法。。");
        while(isRunning == true){
            //...
        }
        System.out.println("线程终止。。");
    }
    public static void main(String args[]) throws InterruptedException{
        RunThread rt = new RunThread();
        rt.start();
        Thread.sleep(3000);
        rt.setRunning(false);
        System.out.println("isRunning的值已经设置成了false");
        Thread.sleep(1000);
        System.out.println(rt.isRunning);
    }
}
```

3. ThreadLocal

> * 线程局部变量，是一种多线程间并发访问变量的解决方案；
> * 与其synchronized等加锁的方式不同，ThreadLocal完全不提供锁，而是用以空间换时间的手段，为每个线程提供变量的独立副本，以保障线程安全；
> * 从性能上说，ThreadLocal不具有绝对优势，在并发不是很高的时候，加锁的性能会更好，但作为一套与锁完全无关的线程安全解决方案，在高并发量或者竞争激烈的场景，使用ThreadLocal可以在一定程度上减少锁竞争。

```java
package com.wp.test;
public class ConnThreadLocal {
    public static ThreadLocal<String> th = new ThreadLocal<String>();
    public void setTh(String value){
        th.set(value);
    }
    public void getTh(){
        System.out.println(Thread.currentThread().getName() + ":" + this.th.get());
    }
    public static void main(String[] args) throws InterruptedException {
        final ConnThreadLocal ct = new ConnThreadLocal();
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                ct.setTh("张三");
                ct.getTh();
            }
        }, "t1");
        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                    //ct.setTh("李四");
                    ct.getTh();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "t2");
        t1.start();
        t2.start();
    }
}
```

4. 单例与多线程

> 单例模式，最常见的就是饥饿模式和懒汉模式，一个是在声明的同时直接实例化对象，一个在调用方法时进行实例化对象。在多线程模式中，考虑到性能和线程安全的问题，我们一般选择duble check instance或static inner class两种比较经典的单例模式，在性能提高的同时，要保证了线程安全。


> * duble check instance

```java
package com.wp.test;
public class DoubleSingleton {
    private static DoubleSingleton ds;
    public static DoubleSingleton getDs(){
        if(ds == null){ // 有判断的话，对象已存在的情况下，不会进行初始化准备
            try {
                //模拟初始化对象的准备时间...
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            synchronized (DoubleSingleton.class) {
                if(ds == null){  // 如果这里没有判断，那么虽然同步的，但是都会创建对象
                    ds = new DoubleSingleton();
                }
            }
        }
        return ds;
    }
    public static void main(String[] args) {
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(DoubleSingleton.getDs().hashCode());
            }
        },"t1");
        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(DoubleSingleton.getDs().hashCode());
            }
        },"t2");
        Thread t3 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(DoubleSingleton.getDs().hashCode());
            }
        },"t3");
        t1.start();
        t2.start();
        t3.start();
    }
}
```

* static inner class

```java
package com.wp.test;
public class InnerSingleton {
	private static class Singletion {
		private static Singletion single = new Singletion();
	}
	public static Singletion getInstance(){
		return Singletion.single;
	}
}
```

5. 同步类容器和并发类容器

> * 同步类容器是线程安全的，如古老的Vector、HashTable

        其底层的机制无非就是用传统的synchronized关键字对每个公用的方法都进行同步，使得每次只能一个线程访问容器的状态。这很明显不满足我们今天互联网时代高并发的需求，在保证线程安全的同时，也必须要有足够好的性能。

> * 并发类容器

        Jdk5.0以后提供了多种并发类容器来替代同步类容器，从而改善了性能。同步类容器的状态都是串行化的。他们虽然实现了线程安全，但是严重降低了并发性，在多线程环境时，严重降低了应用程序的吞吐量。
        并发类容器是专门针对并发设计的，使用ConcurrentHashMap来替代给予散列的传统的HashTable，而且在ConcurrentHashMap中，添加了一些常见复合操作的支持。以及使用了CopyOnWriterArrayList替代Vector，并发的CopyOnWriterArraySet，以及并发的Queue，ConcurrentLinkedQueue和LinkedBlockingQueue，前者是高性能队列，后者是以阻塞形式的队列，具体实现Queue还有很多，例如ArrayBlockingQueue、PriorityBlockingQueue、SynchronousQueue等。

6. ConcurrentHashMap
   
        ConcurrentMap接口下有两个重要的实现：
	    1.ConcurrentHashMap
	    2.ConcurrentSkipListMap（支持并发排序功能，弥补ConcurrentHashMap）

        ConcurrentHashMap内部使用段（Segment）来表示这些不同的部分，每个段其实就是一个小的HashTable，它们有自己的锁。只要多个修改操作发生在不同的段上，它们就可以并发进行。把一个整体分成了16个段（Segment）。也就是最高支持16个线程的并发修改操作。这也是在多线程场景时减小锁的力度从而降低锁竞争的一种方案。并且代码中大多共享变量使用volatile关键字声明，目的是第一时间获取修改的内容，性能非常好。
          创建的时候，内存直接分为了16个segment，每个segment实际上还是存储的哈希表，写入的时候，先找到对应的segment，然后锁这个segment，写完，解锁。


7. Copy-On-Write容器

> JDK里的COW容器有两种：CopyOnWriterArrayList和CopyOnWriterArraySet，COW容器非常有用，可以在非常多的并发场景中使用到。

        CopyOnWriter容器即写时复制的容器。通俗的理解是当我们往一个容器中添加元素的时候，不直接往当前容器添加，而是先将当前容器进行Copy，复制出一个新的容器，然后新的容器里添加元素，添加完元素之后，再将原容器的引用指向新的容器。这样做的好处是我们可以对CopyOnWriter容器进行并发的读，而不需要加锁，因为当前容器不会添加任何元素。所以CopyOnWriter容器也是一种读写分离的思想，读和写不同的容器。
        
    使用场景：读多写少

8. 并发Queue

        在并发队列上JDK提供了两套实现，一个是以ConcurrentLinkedQueue为代表的高性能队列，一个是以BlockingQueue接口为代表的阻塞队列，无论哪种都继承自Queue。

9. ConcurrentLinkedQueue

        ConcurrentLinkedQueue，是一个适合于高并发场景下的队列，通过无锁的方式，实现了高并发状态下的高性能，通常ConcurrentLinkedQueue性能好于BlockingQueue。它是基于链接节点的无界线程安全队列。该队列的元素遵循先进先出的原则。头是最先加入的，尾是最近加入的，该队列不允许null元素。
        ConcurrentLinkedQueue重要方法：
        add()和offer()都是加入元素的方法（在ConcurrentLinkedQueue中，这两个方法没有任何区别）
        poll()和peek()都是取头元素节点，区别在于前者会删除元素，后者不会。

10. BlockingQueue











